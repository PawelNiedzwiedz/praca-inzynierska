﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using Xamarin.Forms.Platform.iOS;

namespace wFirma.iOS.CustomRenderers
{
    public class CustomPickerRenderer : PickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Picker> e)
        {
            base.OnElementChanged(e);
            if (this.Control != null)
            {
                var downarrow = UIImage.FromFile("dropdownArrow.png");
                var textField = this.Control;
                textField.RightViewMode = UITextFieldViewMode.Always;
                textField.RightView = new UIImageView(downarrow);
            }
        }
    }
}