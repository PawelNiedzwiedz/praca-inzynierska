﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

namespace wFirma.iOS.CustomRenderers
{
    public class CustomEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> args)
        {
            base.OnElementChanged(args);

            if (this.Control != null)
            {
                UITextField textField = (UITextField)Control;

                // Most commonly customized for font and no border
                textField.BorderStyle = UITextBorderStyle.None;
                textField.TextColor = UIColor.Black;
                // Use 'Done' on keyboard
                textField.ReturnKeyType = UIReturnKeyType.Done;
                textField.EnablesReturnKeyAutomatically = true;

            }

        }
    }
}