﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using wFirma.CustomControls;
using wFirma.Droid.CustomRenderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
[assembly: ExportRenderer(typeof(CustomPicker), typeof(CustomPickerRenderer))]
namespace wFirma.Droid.CustomRenderers
{
    public class CustomPickerRenderer : PickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Picker> e)
        {
            base.OnElementChanged(e);

            if (this.Control != null)
            {
                try
                {
                    this.Control.Background = Resources.GetDrawable(Resource.Drawable.downarrow);
                    this.Control.TextSize = 14f;
                }
                catch (Exception ex)
                {
                }
            }
        }
    }
}