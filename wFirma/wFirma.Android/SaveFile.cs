﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.IO;
using wFirma.Droid;
using wFirma.Helpers;
using Xamarin.Forms;
using Environment = Android.OS.Environment;
using File = Java.IO.File;

[assembly: Dependency(typeof(SaveAndLoad))]
namespace wFirma.Droid
{
    public class SaveAndLoad : ISaveAndLoad
    {
        public string SaveText(string filename, byte[] text)
        {
//            var documentsPath = Environment.AbsolutePath;
//            var filePath = Path.Combine(documentsPath, filename);
//            System.IO.File.WriteAllBytes(filePath, text);
//            return filePath;
            
            File path = Environment.GetExternalStoragePublicDirectory(Environment.DirectoryDownloads);
            File file = new File(path.AbsolutePath, filename);
            try
            {
                path.Mkdirs();
                OutputStream os = new FileOutputStream(file);
                os.Write(text);
                os.Close();
            }
            catch (Exception Ex)
            {
                
            }
            return file.Name;
        }
        public string LoadText(string filename)
        {
            var documentsPath = Environment.DirectoryDownloads;
            var filePath = Path.Combine(documentsPath, filename);
            return System.IO.File.ReadAllText(filePath);
        }
    }
}
