﻿using System.Collections.Generic;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using wFirma.Models;
using wFirma.Views;
using Xamarin.Forms;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
namespace wFirma
{
    public partial class App : Application
    {
        public static string username;// = "tempkonto6@gmail.com";
        public static string password;// = "pracainzynierska";
        public static string userdata;
        public static List<VatCodeContent> VatCodes;
        public static List<string> VatCodesLabels;
        public static ISettings AppSettings => CrossSettings.Current;

        public App()
        {
            InitializeComponent();
            VatCodes = new List<VatCodeContent>();
            VatCodesLabels = new List<string>();
            MainPage = new RootPage();

        }

        protected override void OnStart()
        {
            AppCenter.Start("android=5ce397fc-b224-4d90-b5dc-3e6be6d82248;" + "uwp={Your UWP App secret here};" +
                            "ios={Your iOS App secret here}",
                typeof(Analytics), typeof(Crashes));
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
