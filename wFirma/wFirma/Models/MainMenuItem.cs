﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wFirma.Models
{
    public class MainMenuItem
    {
        public MainMenuItem()
        {
            MainMenuType = MainMenuType.HomePage;
        }
        public string Title { get; set; }
        public int Id { get; set; }
        public MainMenuType MainMenuType { get; set; }
        public string Icon { get; set; }
       

        
    }

    public enum MainMenuType
    {
        HomePage,
        Contractors,
        Products,
        Invoices,
        Offers,
        Logout
    }
}
