﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace wFirma.Models
{
    [XmlRoot(ElementName = "invoice")]
    public class InvoicePayment
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "expense")]
    public class ExpensePayment
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "declaration_header")]
    public class DeclarationHeader
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "series")]
    public class PaymentSeries
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "payment_cashbox")]
    public class Payment_cashbox
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "contractor")]
    public class ContractorPayment
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "payment")]
    public class Payment
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
        [XmlElement(ElementName = "object_name")]
        public string Object_name { get; set; }
        [XmlElement(ElementName = "object_id")]
        public string Object_id { get; set; }
        [XmlElement(ElementName = "value")]
        public string Value { get; set; }
        [XmlElement(ElementName = "social")]
        public string Social { get; set; }
        [XmlElement(ElementName = "health")]
        public string Health { get; set; }
        [XmlElement(ElementName = "labor_fund")]
        public string Labor_fund { get; set; }
        [XmlElement(ElementName = "transitional_retiring_fund")]
        public string Transitional_retiring_fund { get; set; }
        [XmlElement(ElementName = "date")]
        public string Date { get; set; }
        [XmlElement(ElementName = "initial")]
        public string Initial { get; set; }
        [XmlElement(ElementName = "type")]
        public string Type { get; set; }
        [XmlElement(ElementName = "payment_type")]
        public string Payment_type { get; set; }
        [XmlElement(ElementName = "payment_method")]
        public string Payment_method { get; set; }
        [XmlElement(ElementName = "account")]
        public string Account { get; set; }
        [XmlElement(ElementName = "value_pln")]
        public string Value_pln { get; set; }
        [XmlElement(ElementName = "currency_exchange")]
        public string Currency_exchange { get; set; }
        [XmlElement(ElementName = "currency_label")]
        public string Currency_label { get; set; }
        [XmlElement(ElementName = "currency_date")]
        public string Currency_date { get; set; }
        [XmlElement(ElementName = "tags")]
        public string Tags { get; set; }
        [XmlElement(ElementName = "notes")]
        public string Notes { get; set; }
        [XmlElement(ElementName = "created")]
        public string Created { get; set; }
        [XmlElement(ElementName = "modified")]
        public string Modified { get; set; }
        [XmlElement(ElementName = "invoice")]
        public Invoice Invoice { get; set; }
        [XmlElement(ElementName = "expense")]
        public Expense Expense { get; set; }
        [XmlElement(ElementName = "declaration_header")]
        public DeclarationHeader Declaration_header { get; set; }
        [XmlElement(ElementName = "series")]
        public Series Series { get; set; }
        [XmlElement(ElementName = "payment_cashbox")]
        public Payment_cashbox Payment_cashbox { get; set; }
        [XmlElement(ElementName = "contractor")]
        public Contractor Contractor { get; set; }
    }

    [XmlRoot(ElementName = "parameters")]
    public class PaymentParameters
    {
        [XmlElement(ElementName = "limit")]
        public string Limit { get; set; }
        [XmlElement(ElementName = "page")]
        public string Page { get; set; }
        [XmlElement(ElementName = "total")]
        public string Total { get; set; }
    }

    [XmlRoot(ElementName = "payments")]
    public class Payments
    {
        [XmlElement(ElementName = "payment")]
        public List<Payment> Payment { get; set; }
        [XmlElement(ElementName = "parameters")]
        public PaymentParameters Parameters { get; set; }
    }

    [XmlRoot(ElementName = "status")]
    public class PaymentStatus
    {
        [XmlElement(ElementName = "code")]
        public string Code { get; set; }
    }

    [XmlRoot(ElementName = "api")]
    public class PaymentItem
    {
        [XmlElement(ElementName = "payments")]
        public Payments Payments { get; set; }
        [XmlElement(ElementName = "status")]
        public PaymentStatus Status { get; set; }
    }
}
