﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace wFirma.Models
{
    [XmlRoot(ElementName = "google")]
    public class Google
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "facebook_user")]
    public class Facebook_user
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "user")]
    public class User
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
        [XmlElement(ElementName = "login")]
        public string Login { get; set; }
        [XmlElement(ElementName = "firstname")]
        public string Firstname { get; set; }
        [XmlElement(ElementName = "lastname")]
        public string Lastname { get; set; }
        [XmlElement(ElementName = "phone")]
        public string Phone { get; set; }
        [XmlElement(ElementName = "google")]
        public Google Google { get; set; }
        [XmlElement(ElementName = "facebook_user")]
        public Facebook_user Facebook_user { get; set; }
        public string SerializeUser()
        {
            var user = this;
            return JsonConvert.SerializeObject(user);
        }
    }

    [XmlRoot(ElementName = "users")]
    public class Users
    {
        [XmlElement(ElementName = "user")]
        public User User { get; set; }
    }

    [XmlRoot(ElementName = "status")]
    public class Status
    {
        [XmlElement(ElementName = "code")]
        public string Code { get; set; }
    }

    [XmlRoot(ElementName = "api")]
    public class UserItem
    {
        [XmlElement(ElementName = "users")]
        public Users Users { get; set; }
        [XmlElement(ElementName = "status")]
        public Status Status { get; set; }
    }
}
