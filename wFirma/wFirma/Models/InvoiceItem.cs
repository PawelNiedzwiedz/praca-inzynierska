﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;
using wFirma.Annotations;

namespace wFirma.Models
{
    [XmlRoot(ElementName = "series")]
    public class Series
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "contractor")]
    public class InvoiceContractor
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "contractor_detail")]
    public class ContractorDetail : INotifyPropertyChanged
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
        [XmlElement(ElementName = "tax_id_type")]
        public string TaxIdType { get; set; }
        [XmlElement(ElementName = "name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "nip")]
        public string Nip { get; set; }

        [XmlElement(ElementName = "street")] 
        public string Street { get; set; }

        [XmlElement(ElementName = "zip")]
        public string Zip { get; set; }
        [XmlElement(ElementName = "city")]
        public string City { get; set; }
        [XmlElement(ElementName = "country")]
        public string Country { get; set; }
        [XmlElement(ElementName = "phone")]
        public string Phone { get; set; }
        [XmlElement(ElementName = "email")]
        public string Email { get; set; }
        [XmlElement(ElementName = "discount_percent")]
        public string Discount_percent { get; set; }
        [XmlElement(ElementName = "empty")]
        public string Empty { get; set; }
        [XmlElement(ElementName = "simple")]
        public string Simple { get; set; }
        [XmlElement(ElementName = "created")]
        public string Created { get; set; }
        [XmlElement(ElementName = "modified")]
        public string Modified { get; set; }
        public static explicit operator ContractorDetail(Contractor v)
        {
            ContractorDetail cd = new ContractorDetail();
            cd.Id = v.Id;
            cd.Name = v.Name;
            cd.City = v.City;
            cd.Country = v.Country;
            cd.Nip = v.Nip;
            cd.TaxIdType = v.TaxIdType;
            cd.Street = v.Street;
            cd.Zip = v.Zip;

            return cd;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    [XmlRoot(ElementName = "contractor_receiver")]
    public class ContractorReceiver
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "contractor_detail_receiver")]
    public class ContractorDetailReceiver
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "company_detail")]
    public class CompanyDetail
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
        [XmlElement(ElementName = "name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "altname")]
        public string Altname { get; set; }
        [XmlElement(ElementName = "nip")]
        public string Nip { get; set; }
        [XmlElement(ElementName = "street")]
        public string Street { get; set; }
        [XmlElement(ElementName = "building_number")]
        public string BuildingNumber { get; set; }
        [XmlElement(ElementName = "flat_number")]
        public string FlatNumber { get; set; }
        [XmlElement(ElementName = "zip")]
        public string Zip { get; set; }
        [XmlElement(ElementName = "post")]
        public string Post { get; set; }
        [XmlElement(ElementName = "city")]
        public string City { get; set; }
        [XmlElement(ElementName = "country")]
        public string Country { get; set; }
        [XmlElement(ElementName = "phone")]
        public string Phone { get; set; }
        [XmlElement(ElementName = "email")]
        public string Email { get; set; }
        [XmlElement(ElementName = "bank_name")]
        public string BankName { get; set; }
        [XmlElement(ElementName = "bank_account")]
        public string BankAccount { get; set; }
        [XmlElement(ElementName = "bank_swift")]
        public string BankSwift { get; set; }
        [XmlElement(ElementName = "bank_address")]
        public string Bank_address { get; set; }
        [XmlElement(ElementName = "created")]
        public string Created { get; set; }
        [XmlElement(ElementName = "modified")]
        public string Modified { get; set; }
    }

    [XmlRoot(ElementName = "parent")]
    public class Parent
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "order")]
    public class Order
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "email")]
    public class Email
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "email2")]
    public class Email2
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "text_message")]
    public class Text_message
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "expense")]
    public class Expense
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "company_account")]
    public class CompanyAccount
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "payment_cashbox")]
    public class PaymentCashbox
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "translation_language")]
    public class TranslationLanguage
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "postivo_shipment")]
    public class Postivo_shipment
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "postivo_shipment_content")]
    public class Postivo_shipment_content
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "good_price_group")]
    public class GoodPriceGroup
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "vat_code")]
    public class Vat_code
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "vat_content")]
    public class Vat_content
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
        [XmlElement(ElementName = "object_name")]
        public string Object_name { get; set; }
        [XmlElement(ElementName = "object_id")]
        public string Object_id { get; set; }
        [XmlElement(ElementName = "netto")]
        public string Netto { get; set; }
        [XmlElement(ElementName = "tax")]
        public string Tax { get; set; }
        [XmlElement(ElementName = "brutto")]
        public string Brutto { get; set; }
        [XmlElement(ElementName = "created")]
        public string Created { get; set; }
        [XmlElement(ElementName = "vat_code")]
        public Vat_code Vat_code { get; set; }
    }

    [XmlRoot(ElementName = "vat_contents")]
    public class Vat_contents
    {
        [XmlElement(ElementName = "vat_content")]
        public Vat_content Vat_content { get; set; }
    }

    [XmlRoot(ElementName = "good")]
    public class InvoiceGood
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "invoice")]
    public class Invoice : INotifyPropertyChanged
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
        [XmlElement(ElementName = "interest_status")]
        public string InterestStatus { get; set; }
        [XmlElement(ElementName = "warehouse_type")]
        public string Warehouse_type { get; set; }

        [XmlElement(ElementName = "paymentmethod")]
        public string Paymentmethod { get; set; } = "transfer";
        [XmlElement(ElementName = "paymentdate")]
        public string Paymentdate { get; set; } = DateTime.Now.Date.AddDays(7).ToString("yyyy-MM-dd");

        [XmlElement(ElementName = "paymentstate")]
        public string Paymentstate { get; set; } = "unpaid";
        [XmlElement(ElementName = "disposaldate_format")]
        public string Disposaldate_format { get; set; }
        [XmlElement(ElementName = "disposaldate_empty")]
        public string Disposaldate_empty { get; set; }
        [XmlElement(ElementName = "disposaldate")]
        public string Disposaldate { get; set; } = DateTime.Now.Date.ToString("yyyy-MM-dd");
        [XmlElement(ElementName = "date")]
        public string Date { get; set; } = DateTime.Now.Date.ToString("yyyy-MM-dd");

        [XmlElement(ElementName = "total")]
//        public string Total { get; set; }
        public string _total;
        public string Total
        {
            get => _total;
            set
            {
                _total = value;
                OnPropertyChanged();
            }
        }
        [XmlElement(ElementName = "total_composed")]
        public string Total_composed { get; set; }
        [XmlElement(ElementName = "alreadypaid")]
        public string Alreadypaid { get; set; }
        [XmlElement(ElementName = "alreadypaid_initial")]
        public string AlreadypaidInitial { get; set; }
        [XmlElement(ElementName = "remaining")]
        public string Remaining { get; set; }
        [XmlElement(ElementName = "number")]
        public string Number { get; set; }
        [XmlElement(ElementName = "day")]
        public string Day { get; set; }
        [XmlElement(ElementName = "month")]
        public string Month { get; set; }
        [XmlElement(ElementName = "year")]
        public string Year { get; set; }
        [XmlElement(ElementName = "day_year")]
        public string DayYear { get; set; }
        [XmlElement(ElementName = "fullnumber")]
        public string Fullnumber { get; set; }
        [XmlElement(ElementName = "semitemplatenumber")]
        public string Semitemplatenumber { get; set; }
        [XmlElement(ElementName = "type")]
        public string Type { get; set; }
        [XmlElement(ElementName = "correction_type")]
        public string Correction_type { get; set; }
        [XmlElement(ElementName = "corrections")]
        public string Corrections { get; set; }
        [XmlElement(ElementName = "formal_data_corrections")]
        public string Formal_data_corrections { get; set; }
        [XmlElement(ElementName = "currency")]
        public string Currency { get; set; } = "PLN";
        [XmlElement(ElementName = "currency_exchange")]
        public string Currency_exchange { get; set; }
        [XmlElement(ElementName = "currency_label")]
        public string Currency_label { get; set; }
        [XmlElement(ElementName = "currency_date")]
        public string Currency_date { get; set; }
        [XmlElement(ElementName = "price_currency_exchange")]
        public string Price_currency_exchange { get; set; }
        [XmlElement(ElementName = "good_price_group_currency_exchange")]
        public string Good_price_group_currency_exchange { get; set; }
        [XmlElement(ElementName = "auto_send_postivo")]
        public string Auto_send_postivo { get; set; }
        [XmlElement(ElementName = "auto_send")]
        public string Auto_send { get; set; }
        [XmlElement(ElementName = "account_type")]
        public string Account_type { get; set; }
        [XmlElement(ElementName = "account_date")]
        public string Account_date { get; set; }
        [XmlElement(ElementName = "template")]
        public string Template { get; set; }
        [XmlElement(ElementName = "description")]
        public string Description { get; set; }
        [XmlElement(ElementName = "header")]
        public string Header { get; set; }
        [XmlElement(ElementName = "footer")]
        public string Footer { get; set; }
        [XmlElement(ElementName = "user_name")]
        public string User_name { get; set; }
        [XmlElement(ElementName = "schema")]
        public string Schema { get; set; }
        [XmlElement(ElementName = "schema_vat_cashbox")]
        public string Schema_vat_cashbox { get; set; }
        [XmlElement(ElementName = "schema_vat_cashbox_limit")]
        public string Schema_vat_cashbox_limit { get; set; }
        [XmlElement(ElementName = "schema_vat_cashbox_small_taxpayer")]
        public string Schema_vat_cashbox_small_taxpayer { get; set; }
        [XmlElement(ElementName = "schema_bill")]
        public string Schema_bill { get; set; }
        [XmlElement(ElementName = "schema_receipt_book")]
        public string Schema_receipt_book { get; set; }
        [XmlElement(ElementName = "schema_cancelled")]
        public string Schema_cancelled { get; set; }
        [XmlElement(ElementName = "margin_tax_schema")]
        public string Margin_tax_schema { get; set; }
        [XmlElement(ElementName = "margin_description_schema")]
        public string Margin_description_schema { get; set; }
        [XmlElement(ElementName = "register_description")]
        public string Register_description { get; set; }
        [XmlElement(ElementName = "income_lumpcode")]
        public string Income_lumpcode { get; set; }
        [XmlElement(ElementName = "income_correction")]
        public string Income_correction { get; set; }
        [XmlElement(ElementName = "bill_legal_description")]
        public string Bill_legal_description { get; set; }
        [XmlElement(ElementName = "netto")]
        public string Netto { get; set; }
        [XmlElement(ElementName = "tax")]
        public string Tax { get; set; }
        [XmlElement(ElementName = "receipt_fiscal_printed")]
        public string Receipt_fiscal_printed { get; set; }
        [XmlElement(ElementName = "vindicat_sent")]
        public string Vindicat_sent { get; set; }
        [XmlElement(ElementName = "invipay_sent")]
        public string Invipay_sent { get; set; }
        [XmlElement(ElementName = "courier_shipments")]
        public string Courier_shipments { get; set; }
        [XmlElement(ElementName = "hash")]
        public string Hash { get; set; }
        [XmlElement(ElementName = "id_external")]
        public string Id_external { get; set; }
        [XmlElement(ElementName = "tags")]
        public string Tags { get; set; }
        [XmlElement(ElementName = "notes")]
        public string Notes { get; set; }
        [XmlElement(ElementName = "documents")]
        public string Documents { get; set; }
        [XmlElement(ElementName = "created")]
        public string Created { get; set; }
        [XmlElement(ElementName = "modified")]
        public string Modified { get; set; }
        [XmlElement(ElementName = "price_type")]
        public string Price_type { get; set; }
        [XmlElement(ElementName = "series")]
        public Series Series { get; set; }
        [XmlElement(ElementName = "contractor")]
        public Contractor Contractor { get; set; }

        [XmlElement(ElementName = "contractor_detail")]
        public ContractorDetail _contractorDetail;
        public ContractorDetail ContractorDetail
        {
            get => _contractorDetail;
            set { _contractorDetail = value;
            OnPropertyChanged();
            }
        }
        [XmlElement(ElementName = "contractor_receiver")]
        public ContractorReceiver Contractor_receiver { get; set; }
        [XmlElement(ElementName = "contractor_detail_receiver")]
        public ContractorDetailReceiver Contractor_detail_receiver { get; set; }
        [XmlElement(ElementName = "company_detail")]
        public CompanyDetail CompanyDetail { get; set; }
        [XmlElement(ElementName = "parent")]
        public Parent Parent { get; set; }
        [XmlElement(ElementName = "order")]
        public Order Order { get; set; }
        [XmlElement(ElementName = "email")]
        public Email Email { get; set; }
        [XmlElement(ElementName = "email2")]
        public Email2 Email2 { get; set; }
        [XmlElement(ElementName = "text_message")]
        public Text_message Text_message { get; set; }
        [XmlElement(ElementName = "expense")]
        public Expense Expense { get; set; }
        [XmlElement(ElementName = "company_account")]
        public Company_account Company_account { get; set; }
        [XmlElement(ElementName = "payment_cashbox")]
        public PaymentCashbox Payment_cashbox { get; set; }
        [XmlElement(ElementName = "translation_language")]
        public Translation_language Translation_language { get; set; }
        [XmlElement(ElementName = "postivo_shipment")]
        public Postivo_shipment Postivo_shipment { get; set; }
        [XmlElement(ElementName = "postivo_shipment_content")]
        public Postivo_shipment_content Postivo_shipment_content { get; set; }
        [XmlElement(ElementName = "good_price_group")]
        public Good_price_group Good_price_group { get; set; }
        [XmlElement(ElementName = "vat_contents")]
        public Vat_contents Vat_contents { get; set; }

        [XmlElement(ElementName = "invoicecontents")]
//        public Invoicecontents _invoicecontents;
        public Invoicecontents InvoiceContents { get; set; }
//        public Invoicecontents InvoiceContents
//        {
//            get => _invoicecontents;
//            set
//            {
//                _invoicecontents = value;
//                OnPropertyChanged();
//            }
//        }
        [XmlIgnore]
        public List<string> PaymentMethods { get; set; } =
            new List<string> { "gotówka", "przelew", "kompensata", "za pobraniem", "karta płatnicza" };

        [XmlIgnore]
        public bool EditMode { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    [XmlRoot(ElementName = "fixed_asset")]
    public class Fixed_asset
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "equipment")]
    public class Equipment
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "warehouse_document_content")]
    public class Warehouse_document_content
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "invoicecontent")]
    public class Invoicecontent : INotifyPropertyChanged
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
        [XmlElement(ElementName = "name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "classification")]
        public string Classification { get; set; }
        [XmlElement(ElementName = "unit")]
        public string Unit { get; set; }

        [XmlElement(ElementName = "count")]
        public string _count;

        public string Count
        {
            get => _count;
            set
            {
                _count = value;
                OnPropertyChanged();
            }
        }
        [XmlElement(ElementName = "price")]
        public string Price { get; set; }
        [XmlElement(ElementName = "price_modified")]
        public string Price_modified { get; set; }
        [XmlElement(ElementName = "discount")]
        public string Discount { get; set; }
        [XmlElement(ElementName = "discount_percent")]
        public string Discount_percent { get; set; }
        [XmlElement(ElementName = "netto")]
        public string Netto { get; set; }

        [XmlElement(ElementName = "brutto")]
        public string _brutto;

        public string Brutto
        {
            get => _brutto;
            set
            {
                _brutto = value;
                OnPropertyChanged();
            }
        }
        [XmlElement(ElementName = "lumpcode")]
        public string Lumpcode { get; set; }
        [XmlElement(ElementName = "created")]
        public string Created { get; set; }
        [XmlElement(ElementName = "modified")]
        public string Modified { get; set; }
        [XmlElement(ElementName = "good")]
        public Good Good { get; set; }
        [XmlElement(ElementName = "invoice")]
        public Invoice Invoice { get; set; }

        [XmlElement(ElementName = "vat_code")]
        public VatCode VatCode { get; set; } = null;
        [XmlElement(ElementName = "fixed_asset")]
        public Fixed_asset Fixed_asset { get; set; }
        [XmlElement(ElementName = "equipment")]
        public Equipment Equipment { get; set; }
        [XmlElement(ElementName = "warehouse_document_content")]
        public Warehouse_document_content Warehouse_document_content { get; set; }
        [XmlElement(ElementName = "parent")]
        public Parent Parent { get; set; }

        [XmlIgnore]
        public string VatCodeLabel { get; set; } = "";

        [XmlIgnore]
        public int _productCount;
        public int ProductCount
        {
            get => _productCount;
            set
            {
                _productCount = value;
                OnPropertyChanged();
            }
        }
        public static explicit operator Invoicecontent(Good product)
        {
            Invoicecontent ic = new Invoicecontent();
            ic.Id = product.Id;
            
            var productCount = product.Count.Remove(product.Count.IndexOf('.'));
            var count = Convert.ToInt32(productCount);
            var productBrutto = product.Brutto.Replace('.', ',');
            var brutto = Convert.ToDouble(productBrutto);
            var totalBrutto = product.ProductCount * brutto;
            ic.Brutto = totalBrutto.ToString("##.###").Replace(",", ".");
            ic.Price = product.Netto;
            ic.Classification = product.Classification;
            ic.ProductCount = 1;
            ic.Count = productCount;
            ic.Unit = product.Unit;         
            ic.Discount = product.Discount;
            ic.Name = product.Name;
            ic.Lumpcode = product.Lumpcode;
            ic.VatCode = new VatCode {Id = product.VatCode.Id};
            ic.VatCodeLabel = product.VatLabel;
            
            return ic;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    [XmlRoot(ElementName = "invoicecontents")]
    public class Invoicecontents
    {
        [XmlElement(ElementName = "invoicecontent")]
        public ObservableCollection<Invoicecontent> Invoicecontent { get; set; }
    }

    [XmlRoot(ElementName = "parameters")]
    public class InvoiceParameters
    {
        [XmlElement(ElementName = "limit")]
        public string Limit { get; set; }
        [XmlElement(ElementName = "page")]
        public string Page { get; set; }
        [XmlElement(ElementName = "total")]
        public string Total { get; set; }
    }

    [XmlRoot(ElementName = "invoices")]
    public class Invoices
    {
        [XmlElement(ElementName = "invoice")]
        public List<Invoice> Invoice { get; set; }
        [XmlElement(ElementName = "parameters")]
        public InvoiceParameters Parameters { get; set; }
    }

    [XmlRoot(ElementName = "status")]
    public class InvoiceStatus
    {
        [XmlElement(ElementName = "code")]
        public string Code { get; set; }
        [XmlElement(ElementName = "message")]
        public string Message { get; set; }
    }

    [XmlRoot(ElementName = "api")]
    public class InvoiceItem
    {
        [XmlElement(ElementName = "invoices")]
        public Invoices Invoices { get; set; }
        [XmlElement(ElementName = "status")]
        public InvoiceStatus Status { get; set; }
    }
}
