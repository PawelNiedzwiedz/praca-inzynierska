﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace wFirma.Models
{
    [XmlRoot(ElementName = "parameter")]
    public class PdfParameter
    {
        [XmlElement(ElementName = "name")]
        public string Name { get; set; }

        [XmlElement(ElementName = "value")]
        public string Value { get; set; }
    }

    [XmlRoot(ElementName = "parameters")]
    public class PdfParameters
    {
        [XmlElement(ElementName = "parameter")]
        public List<PdfParameter> Parameter { get; set; }
    }

    [XmlRoot(ElementName = "invoices")]
    public class PdfInvoices
    {
        [XmlElement(ElementName = "parameters")]
        public PdfParameters Parameters { get; set; }
    }

    [XmlRoot(ElementName = "api")]
    public class PdfPreviewItem
    {
        [XmlElement(ElementName = "invoices")]
        public PdfInvoices Invoices { get; set; }
    }
}

