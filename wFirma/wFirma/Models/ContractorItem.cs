﻿
using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using wFirma.Extensions;

namespace wFirma.Models
{
    [XmlRoot(ElementName = "reference_company")]
    public class Reference_company
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "translation_language")]
    public class Translation_language
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "company_account")]
    public class Company_account
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "good_price_group")]
    public class Good_price_group
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "invoice_description")]
    public class Invoice_description
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "shop_buyer")]
    public class Shop_buyer
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "contractor")]
    public class Contractor
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }

        [XmlElement(ElementName = "tax_id_type")]
        public string TaxIdType { get; set; } = "nip";
        [XmlElement(ElementName = "name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "altname")]
        public string AltName { get; set; }
        [XmlElement(ElementName = "nip")]
        public string Nip { get; set; }
        [XmlElement(ElementName = "regon")]
        public string Regon { get; set; }
        [XmlElement(ElementName = "street")]
        public string Street { get; set; }
        [XmlElement(ElementName = "zip")]
        public string Zip { get; set; }
        [XmlElement(ElementName = "city")]
        public string City { get; set; }

        [XmlElement(ElementName = "country")]
        public string Country { get; set; } = "PL";

        [XmlElement(ElementName = "different_contact_address")]
        public string DifferentContactAddress { get; set; } = "0";
        [XmlElement(ElementName = "contact_name")]
        public string ContactName { get; set; }
        [XmlElement(ElementName = "contact_street")]
        public string ContactStreet { get; set; }
        [XmlElement(ElementName = "contact_zip")]
        public string ContactZip { get; set; }
        [XmlElement(ElementName = "contact_city")]
        public string ContactCity { get; set; }

        [XmlElement(ElementName = "contact_country")]
        public string ContactCountry { get; set; } = "PL";
        [XmlElement(ElementName = "contact_person")]
        public string ContactPerson { get; set; }
        [XmlElement(ElementName = "phone")]
        public string Phone { get; set; }
        [XmlElement(ElementName = "skype")]
        public string Skype { get; set; }
        [XmlElement(ElementName = "fax")]
        public string Fax { get; set; }
        [XmlElement(ElementName = "email")]
        public string Email { get; set; }
        [XmlElement(ElementName = "url")]
        public string Url { get; set; }
        [XmlElement(ElementName = "description")]
        public string Description { get; set; }
        [XmlElement(ElementName = "buyer")]
        public string Buyer { get; set; }
        [XmlElement(ElementName = "seller")]
        public string Seller { get; set; }
        [XmlElement(ElementName = "discount_percent")]
        public string Discount_percent { get; set; }
        [XmlElement(ElementName = "payment_days")]
        public string Payment_days { get; set; }
        [XmlElement(ElementName = "payment_method")]
        public string Payment_method { get; set; }
        [XmlElement(ElementName = "account_number")]
        public string Account_number { get; set; }
        [XmlElement(ElementName = "remind")]
        public string Remind { get; set; }
        [XmlElement(ElementName = "hash")]
        public string Hash { get; set; }
        [XmlElement(ElementName = "avatar_filename")]
        public string AvatarFilename { get; set; }
        [XmlElement(ElementName = "source")]
        public string Source { get; set; }
        [XmlElement(ElementName = "tags")]
        public string Tags { get; set; }
        [XmlElement(ElementName = "notes")]
        public string Notes { get; set; }
        [XmlElement(ElementName = "documents")]
        public string Documents { get; set; }
        [XmlElement(ElementName = "created")]
        public string Created { get; set; }
        [XmlElement(ElementName = "modified")]
        public string Modified { get; set; }
        [XmlElement(ElementName = "reference_company")]
        public Reference_company ReferenceCompany { get; set; }
        [XmlElement(ElementName = "translation_language")]
        public Translation_language TranslationLanguage { get; set; }
        [XmlElement(ElementName = "company_account")]
        public Company_account CompanyAccount { get; set; }
        [XmlElement(ElementName = "good_price_group")]
        public Good_price_group GoodPriceGroup { get; set; }
        [XmlElement(ElementName = "invoice_description")]
        public Invoice_description InvoiceDescription { get; set; }
        [XmlElement(ElementName = "shop_buyer")]
        public Shop_buyer Shop_buyer { get; set; }

        [XmlIgnore]
        public Boolean IsEditable { get; set; } = true;
        [XmlIgnore]
        public List<string> TaxIdTypes { get; set; } = new List<string>(){"NIP", "VAT EU", "PESEL", "REGON", "Inny", "Brak"};     
        
    }

    [XmlRoot(ElementName = "parameters")]
    public class Parameters
    {
        [XmlElement(ElementName = "limit")]
        public string Limit { get; set; }
        [XmlElement(ElementName = "page")]
        public string Page { get; set; }
        [XmlElement(ElementName = "total")]
        public string Total { get; set; }
    }

    [XmlRoot(ElementName = "contractors")]
    public class Contractors
    {
        [XmlElement(ElementName = "contractor")]
        public List<Contractor> Contractor { get; set; }
        [XmlElement(ElementName = "parameters")]
        public Parameters Parameters { get; set; }
    }

    [XmlRoot(ElementName = "status")]
    public class ContractorStatus
    {
        [XmlElement(ElementName = "code")]
        public string Code { get; set; }
        [XmlElement(ElementName = "message")]
        public string Message { get; set; }
    }

    [XmlRoot(ElementName = "api")]
    public class ContractorItem
    {
        [XmlElement(ElementName = "contractors")]
        public Contractors Contractors { get; set; }
        [XmlElement(ElementName = "status")]
        public ContractorStatus Status { get; set; }
    }

}