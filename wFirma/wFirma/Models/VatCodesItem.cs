﻿using System.Collections.Generic;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace wFirma.Models
{
    [XmlRoot(ElementName = "declaration_country")]
    public class Declaration_country
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "vat_code_period")]
    public class Vat_code_period
    {
        [XmlElement(ElementName = "start")]
        public string Start { get; set; }
        [XmlElement(ElementName = "stop")]
        public string Stop { get; set; }
    }

    [XmlRoot(ElementName = "vat_code_periods")]
    public class Vat_code_periods
    {
        [XmlElement(ElementName = "vat_code_period")]
        public Vat_code_period Vat_code_period { get; set; }
    }

    [XmlRoot(ElementName = "vat_code")]
    public class VatCodeContent
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
        [XmlElement(ElementName = "label")]
        public string Label { get; set; }
        [XmlElement(ElementName = "rate")]
        public double Rate { get; set; }
        [XmlElement(ElementName = "code")]
        public string Code { get; set; }
        [XmlElement(ElementName = "type")]
        public string Type { get; set; }
        [XmlElement(ElementName = "priority")]
        public string Priority { get; set; }
        [XmlElement(ElementName = "created")]
        public string Created { get; set; }
        [XmlElement(ElementName = "modified")]
        public string Modified { get; set; }
        [XmlElement(ElementName = "declaration_country")]
        public Declaration_country Declaration_country { get; set; }
        [XmlElement(ElementName = "vat_code_periods")]
        public Vat_code_periods Vat_code_periods { get; set; }

        public string SerializeVatContent()
        {
            var vatCode = this;
            return JsonConvert.SerializeObject(vatCode);
        }
    }

    [XmlRoot(ElementName = "parameters")]
    public class VatCodesParameters
    {
        [XmlElement(ElementName = "limit")]
        public string Limit { get; set; }
        [XmlElement(ElementName = "page")]
        public string Page { get; set; }
        [XmlElement(ElementName = "total")]
        public string Total { get; set; }
    }

    [XmlRoot(ElementName = "vat_codes")]
    public class VatCodes
    {
        [XmlElement(ElementName = "vat_code")]
        public List<VatCodeContent> Vat_code { get; set; }
        [XmlElement(ElementName = "parameters")]
        public VatCodesParameters Parameters { get; set; }
    }

    [XmlRoot(ElementName = "status")]
    public class VatCodeStatus
    {
        [XmlElement(ElementName = "code")]
        public string Code { get; set; }
    }

    [XmlRoot(ElementName = "api")]
    public class VatCodesItem
    {
        [XmlElement(ElementName = "vat_codes")]
        public VatCodes VatCodes { get; set; }
        [XmlElement(ElementName = "status")]
        public VatCodeStatus Status { get; set; }
    }
}
