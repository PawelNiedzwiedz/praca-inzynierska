﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace wFirma.Models
{
    [XmlRoot(ElementName = "vat_code")]
    public class VatCode
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; } = "222";
    }

    [XmlRoot(ElementName = "warehouse")]
    public class Warehouse
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "good")]
    public class Good
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
        [XmlElement(ElementName = "name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "code")]
        public string Code { get; set; }

        [XmlElement(ElementName = "unit")]
        public string Unit { get; set; } = "szt.";

        [XmlElement(ElementName = "netto")]
        public string Netto { get; set; } = "";

        [XmlElement(ElementName = "brutto")]
        public string Brutto { get; set; } = "";
        [XmlElement(ElementName = "lumpcode")]
        public string Lumpcode { get; set; }

        [XmlElement(ElementName = "type")]
        public string Type { get; set; } = "good";
        [XmlElement(ElementName = "classification")]
        public string Classification { get; set; }

        [XmlElement(ElementName = "discount")]
        public string Discount { get; set; } = "0";
        [XmlElement(ElementName = "description")]
        public string Description { get; set; }
        [XmlElement(ElementName = "tags")]
        public string Tags { get; set; }
        [XmlElement(ElementName = "notes")]
        public string Notes { get; set; }
        [XmlElement(ElementName = "documents")]
        public string Documents { get; set; }
        [XmlElement(ElementName = "created")]
        public string Created { get; set; }
        [XmlElement(ElementName = "modified")]
        public string Modified { get; set; }

        [XmlElement(ElementName = "count")]
        public string Count { get; set; }
        [XmlElement(ElementName = "reserved")]
        public string Reserved { get; set; }
        [XmlElement(ElementName = "min")]
        public string Min { get; set; }
        [XmlElement(ElementName = "max")]
        public string Max { get; set; }
        [XmlElement(ElementName = "secure")]
        public string Secure { get; set; }
        [XmlElement(ElementName = "visibility")]
        public string Visibility { get; set; }
        [XmlElement(ElementName = "warehouse_type")]
        public string WarehouseType { get; set; }

        [XmlElement(ElementName = "price_type")]
        public string PriceType { get; set; } = "netto";

        [XmlElement(ElementName = "vat_code")]
        public VatCode VatCode { get; set; }
        [XmlElement(ElementName = "warehouse")]
        public Warehouse Warehouse { get; set; }
        [XmlIgnore]
        public Boolean IsEditable { get; set; }
        [XmlIgnore]
        public Boolean IsSelected { get; set; }

        [XmlIgnore]
        public string VatLabel { get; set; } 

        [XmlIgnore]
        public double VatRate { get; set; }
        [XmlIgnore]
        public List<string> VatCodesList { get; set; } = new List<string>(App.VatCodesLabels);

        [XmlIgnore]
        public int ProductCount { get; set; } = 1;
        [XmlIgnore]
        public string ProductIcon { get; set; }
    }
    [XmlRoot(ElementName = "goods")]
    public class Goods
    {
        [XmlElement(ElementName = "good")]
        public List<Good> Good { get; set; }
        [XmlElement(ElementName = "parameters")]
        public Parameters Parameters { get; set; }
    }
    [XmlRoot(ElementName = "status")]
    public class ProductStatus
    {
        [XmlElement(ElementName = "code")]
        public string Code { get; set; }
        [XmlElement(ElementName = "message")]
        public string Message { get; set; }
    }
    [XmlRoot(ElementName = "api")]
    public class ProductItem
    {
        [XmlElement(ElementName = "goods")]
        public Goods Goods { get; set; }
        [XmlElement(ElementName = "status")]
        public ProductStatus Status { get; set; }

    }

}
