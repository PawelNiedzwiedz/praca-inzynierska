﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Syncfusion.ListView.XForms;
using wFirma.Models;
using Xamarin.Forms;

namespace wFirma.Converters
{
   
    public class BoolToProductTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((string)value).Equals("service") ? true : false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? "service" : "good";
        }
    }
    public class BoolToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ((value as string).Equals("1")) ? true : false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (bool)value ? "1" : "0";
        }
    }
    public class PaymentMethodsReplacer : IValueConverter
    {
        Dictionary<string, string> paymentMethods = new Dictionary<string, string>() { { "cash", "gotówka" }, { "transfer", "przelew" }, { "compensation", "kompensata" }, { "cod", "za pobraniem" }, { "payment_card", "karta płatnicza" } };

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            foreach (var type in paymentMethods)
            {
                if (value.Equals(type.Key))
                {
                    return type.Value;
                }
            }
            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            foreach (var type in paymentMethods)
            {
                if (value.Equals(type.Value))
                {
                    return type.Key;
                }
            }
            return "";
        }
    }
    public class PaymentStateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((string)value).Equals("paid") ? true : false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? "paid" : "unpaid";
        }
    }
    public class PaymentStateReplacer : IValueConverter
    {
        Dictionary<string, string> PaymentStates = new Dictionary<string, string>() { { "paid", "opłacona" }, { "unpaid", "nieopłacona" }};

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            foreach (var type in PaymentStates)
            {
                if (value.Equals(type.Key))
                {
                    return type.Value;
                }
            }
            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            foreach (var type in PaymentStates)
            {
                if (value.Equals(type.Value))
                {
                    return type.Key;
                }
            }
            return "";
        }
    }
    public class PaymentStateColor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (value.ToString().ToLower())
            {
                case "paid":
                    return Color.Green;
                case "unpaid":
                    return Color.Red;
            }

            return Color.Gray;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class TaxIdTypeReplacer : IValueConverter
    {
        Dictionary<string, string> TaxIdTypes = new Dictionary<string, string>() { { "nip", "NIP" }, { "vat", "VAT EU" }, { "pesel", "PESEL" }, { "regon", "REGON" }, { "custom", "Inny" }, { "none", "Brak" } };

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            foreach (var type in TaxIdTypes)
            {
                if (value.Equals(type.Key))
                {
                    return type.Value;
                }
            }
            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            foreach (var type in TaxIdTypes)
            {
                if (value.Equals(type.Value))
                {
                    return type.Key;
                }
            }
            return "";
        }
    }
    public class VatCodesReplacer : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            foreach (var code in App.VatCodes)
            {
                if (value.Equals(code.Id))
                {
                    return code.Label;
                }
            }
            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            foreach (var code in App.VatCodes)
            {
                if (value.Equals(code.Label))
                {
                    return code.Id;
                }
            }
            return "";
        }
    }
}
