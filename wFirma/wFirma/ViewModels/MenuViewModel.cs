﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using wFirma.Annotations;
using wFirma.Models;

namespace wFirma.ViewModels
{
    public class MenuViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<MainMenuItem> MenuItems { get; set; }
       
        public MenuViewModel()
        {
            
            MenuItems = new ObservableCollection<MainMenuItem>();
            MenuItems.Add(new MainMenuItem
            {
                Id = 0,
                Title = "Strona główna",
                MainMenuType = MainMenuType.HomePage,
                Icon = "homepage.png"
            });
            MenuItems.Add(new MainMenuItem
            {
                Id = 1,
                Title = "Kontrahenci", 
                MainMenuType = MainMenuType.Contractors, 
                Icon = "contractors.png"
            });
            MenuItems.Add(new MainMenuItem
            {
                Id = 2,
                Title = "Produkty", 
                MainMenuType = MainMenuType.Products, 
                Icon = "products.png"
            });
            MenuItems.Add(new MainMenuItem
            {
                Id = 3,
                Title = "Faktury",
                MainMenuType = MainMenuType.Invoices,
                Icon = "invoices.png"
            });
            MenuItems.Add(new MainMenuItem
            {
                Id = 4,
                Title = "Oferty",
                MainMenuType = MainMenuType.Offers,
                Icon = "offers.png"
            });
        }

        private string title = string.Empty;
        private string icon = null;
        private bool isBusy;
        private bool isNotBusy = true;

        public string Title
        {
            get => title;
            set => title = value;
        }

        public string Icon
        {
            get => icon;
            set => icon = value;
        }

        public bool IsBusy
        {
            get => isBusy;
            set
            {
                if (isBusy == value)
                {
                    isNotBusy = !isBusy;
                }
            }
        }

        public bool IsNotBusy
        {
            get => isNotBusy;
            set => isNotBusy = value;
        }
        public string UserData { get; set; }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
