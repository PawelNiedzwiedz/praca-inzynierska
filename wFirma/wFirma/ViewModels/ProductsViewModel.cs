﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml.Serialization;
using Acr.UserDialogs;
using ModernHttpClient;
using wFirma.Annotations;
using wFirma.Extensions;
using wFirma.Models;
using Xamarin.Forms;

namespace wFirma.ViewModels
{
    public class ProductsViewModel
    {
        private readonly HttpClient _client;
        private string _title = String.Empty;
        private string _icon = null;
        private bool _isBusy;
        private bool _isNotBusy = true;
        private bool _isRefreshing = false;
        private Command _loadItemsCommand;
        private Command _deleteItemCommand;
        private Command _addItemCommand;
        private Command _editItemCommand;
        private ICommand _refreshCommand;
        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<Good> _products;
        private ObservableCollection<SortedProductList> _groups;
        private Good _product;
        private Good _selectedProductItem;
        public ProductsViewModel()
        {
            var authData = String.Format("{0}:{1}", App.username, App.password);
            var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));
            _client = new HttpClient(new NativeMessageHandler());
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
            _products = new ObservableCollection<Good>();
            _groups = new ObservableCollection<SortedProductList>();
        }
        public string Title
        {
            get => _title;
            set => _title = value;
        }
        public string Icon
        {
            get => _icon;
            set => _icon = value;
        }
        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                if (_isBusy == value)
                {
                    return;
                }
                _isBusy = value;
                _isNotBusy = !_isBusy;
                OnPropertyChanged(nameof(IsBusy));
            }
        }
        public bool IsNotBusy
        {
            get => _isNotBusy;
            set => _isNotBusy = value;
        }
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                _isRefreshing = value;
                OnPropertyChanged(nameof(IsRefreshing));
            }
        }
        public ObservableCollection<Good> Products
        {
            get => this._products;
            set
            {
                if (_products != value)
                {
                    _products = value;
                    OnPropertyChanged(nameof(Products));
                }
            }
        }

        public ObservableCollection<SortedProductList> Groups
        {
            get => this._groups;
            set
            {
                if (_groups != value)
                {
                    _groups = value;
                    OnPropertyChanged(nameof(Groups));
                }
            }
        }

        public Good SelectedProductItem
        {
            get { return _selectedProductItem; }
            set
            {
                _selectedProductItem = value;
                OnPropertyChanged();
            }
        }
        public Good GetProductItem(string id)
        {
            return Products.FirstOrDefault(x => x.Id.Equals(id));
        }
        public Good Product
        {
            get { return _product; }
            set
            {
                _product = value;
                OnPropertyChanged();
            }
        }
        public Command LoadItemsCommand
        {
            get
            {
                return _loadItemsCommand ?? (_loadItemsCommand =
                           new Command(async () =>
                           {
                               UserDialogs.Instance.ShowLoading("Proszę czekać...", MaskType.Gradient);
                               await GetProductsData();
                               UserDialogs.Instance.HideLoading();
                           }));
            }
        }

        public Command EditItemCommand
        {
            get { return _editItemCommand ?? (_editItemCommand = new Command(async (productId) => await EditProductData((String)productId))); }
        }
        public Command RemoveItemCommand
        {
            get { return _deleteItemCommand ?? (_deleteItemCommand = new Command(async (productId) => await RemoveProduct((String)productId))); }

        }

        public Command AddItemCommand
        {
            get { return _addItemCommand ?? (_addItemCommand = new Command(async (product) => await AddProduct((Good)product))); }

        }
        public async Task GetProductsData()
        {
            if (_isBusy)
            {
                return;
            }
            _isBusy = true;
            var error = false;
            try
            {
                var response = await _client.GetAsync("https://api2.wfirma.pl/goods/find?outputFormat=xml");
                var generatedXml = await response.Content.ReadAsStringAsync();
                var objectResult = new ProductItem();
                if (generatedXml != "")
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ProductItem));
                    using (TextReader reader = new StringReader(generatedXml))
                    {
                        objectResult = (ProductItem)serializer.Deserialize(reader);
                    }
                }
                var goodGroup = new SortedProductList() { Title = "Towar", ShortTitle = "T" };
                var serviceGroup = new SortedProductList() { Title = "Usługa", ShortTitle = "U" };
                _products.Clear();
                _groups.Clear();
                goodGroup.Clear();
                serviceGroup.Clear();
                foreach (var product in objectResult.Goods.Good)
                {
                    foreach (var vatCode in App.VatCodes)
                    {
                        if (!vatCode.Id.Equals(product.VatCode.Id)) continue;
                        product.VatLabel = vatCode.Label;
                        product.VatRate = vatCode.Rate;
                    }
                    if (product.Type.Equals("good"))
                    {
                        product.ProductIcon = "product.png";
                        goodGroup.Add(product);
                    }
                    else
                    {
                        product.ProductIcon = "service.png";
                        serviceGroup.Add(product);
                    }
                }
                Groups.Add(goodGroup);
                Groups.Add(serviceGroup);
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Debug.WriteLine(ex.StackTrace);
                error = true;
            }
            if (error)
            {
                var page = new ContentPage();
                var result = page.DisplayAlert("Błąd", "Wystąpił błąd podczas ładowania listy produktów.", "OK");
            }
            _isBusy = false;
        }

        public async Task EditProductData(string productId)
        {
            var error = false;
            UserDialogs.Instance.ShowLoading("Proszę czekać...", MaskType.Gradient);
            try
            {
                var editedProduct = GetProductItem(productId);
                ProductItem objectResult = new ProductItem();
                objectResult.Goods = new Goods();
                objectResult.Goods.Good= new List<Good>();
                objectResult.Goods.Good.Add(editedProduct);
                XmlSerializer xml = new XmlSerializer(typeof(ProductItem));
                Utf8StringWriter text = new Utf8StringWriter();
                xml.Serialize(text, objectResult);
                var content = new StringContent(text.ToString());
                var response = await _client.PostAsync("https://api2.wfirma.pl/goods/edit/" + productId, content);
                var status = response.StatusCode;
                if (status == HttpStatusCode.OK)
                {
                    UserDialogs.Instance.Alert(new AlertConfig()
                        .SetMessage("Modyfikacja produktu przebiegła pomyślnie.")
                        .SetTitle("Sukces")
                        .SetOkText("OK"));
                }

            }
            catch
            {
                error = true;
            }
            if (error)
            {
                var page = new ContentPage();
                var result = page.DisplayAlert("Błąd", "Wystąpił błąd modyfikowania kontrahenta.", "OK");
            }
            UserDialogs.Instance.HideLoading();
        }

        public async Task RemoveProduct(string productId)
        {
            var result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig()
                .SetMessage("Czy chcesz usunąć wybrany produkt?")
                .SetTitle("Usuń produkt")
                .SetCancelText("Nie")
                .SetOkText("Tak"));
            if (result)
            {
                UserDialogs.Instance.ShowLoading("Proszę czekać...", MaskType.Gradient);
                await Task.Delay(1000);
                _isBusy = true;
                var query = await _client.GetAsync("https://api2.wfirma.pl/goods/delete/" + productId+ "?outputFormat=xml");
                var generatedXml = await query.Content.ReadAsStringAsync();
                var objectResult = new ProductItem();
                if (generatedXml != null)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ProductItem));
                    using (TextReader reader = new StringReader(generatedXml))
                    {
                        objectResult = (ProductItem)serializer.Deserialize(reader);
                    }
                }
                var product = GetProductItem(productId);
                Products.Remove(product);
                switch (objectResult.Status.Code)
                {
                    case "NOT FOUND":
                        UserDialogs.Instance.Alert(new AlertConfig()
                            .SetMessage("Podany obiekt nie istnieje")
                            .SetTitle("Błąd")
                            .SetOkText("OK"));
                        break;
                    case "OK":
                        UserDialogs.Instance.Alert(new AlertConfig()
                            .SetMessage(objectResult.Status.Message)
                            .SetTitle("Sukces")
                            .SetOkText("OK"));
                        break;
                }
                _isBusy = false;
                UserDialogs.Instance.HideLoading();
            }
        }

        public async Task AddProduct(Good newProduct)
        {
            var error = false;
            UserDialogs.Instance.ShowLoading("Proszę czekać...", MaskType.Gradient);
            try
            {
                ProductItem objectResult = new ProductItem();
                objectResult.Goods = new Goods();
                objectResult.Goods.Good = new List<Good>();
                objectResult.Goods.Good.Add(newProduct);
                XmlSerializer xml = new XmlSerializer(typeof(ProductItem));
                Utf8StringWriter text = new Utf8StringWriter();
                xml.Serialize(text, objectResult);
                var content = new StringContent(text.ToString());
                var response = await _client.PostAsync("https://api2.wfirma.pl/goods/add/", content);
                var status = response.StatusCode;
                if (status == HttpStatusCode.OK)
                {
                    UserDialogs.Instance.Alert(new AlertConfig()
                        .SetMessage("Dodanie nowego produktu przebiegło pomyślnie")
                        .SetTitle("Sukces")
                        .SetOkText("OK"));
                }
                Products.Add(newProduct);
            }
            catch
            {
                error = true;
            }
            if (error)
            {
                var page = new ContentPage();
                var result = page.DisplayAlert("Błąd", "Wystąpił błąd modyfikowania produktu.", "OK");
            }
            UserDialogs.Instance.HideLoading();
        }

        public ICommand RefreshCommand
        {
            get
            {
                return _refreshCommand ??
                       (_refreshCommand = new Command(async () =>
                       {
                           IsRefreshing = true;
                           await GetProductsData();
                           IsRefreshing = false;
                       }));
            }
        }
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        }
    }
}
