﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Acr.UserDialogs;
using ModernHttpClient;
using wFirma.Annotations;
using wFirma.Models;
using Xamarin.Forms;

namespace wFirma.ViewModels
{
    public class HomePageViewModel : INotifyPropertyChanged
    {
        private Command _getVatCodesCommand;
        private string _title = string.Empty;
        private string _icon = null;
        private bool _isBusy;
        private bool _isNotBusy = true;
        public event PropertyChangedEventHandler PropertyChanged;
        private HttpClient _client;
        public HomePageViewModel()
        {
            var authData = string.Format("{0}:{1}", App.username, App.password);
            var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));
            _client = new HttpClient(new NativeMessageHandler());
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
        }
        public string Title
        {
            get => _title;
            set => _title = value;
        }
        public string Icon
        {
            get => _icon;
            set => _icon = value;
        }
        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                if (_isBusy == value)
                {
                    return;
                }
                _isBusy = value;
                _isNotBusy = !_isBusy;
                OnPropertyChanged(nameof(IsBusy));
            }
        }
        public bool IsNotBusy
        {
            get => _isNotBusy;
            set => _isNotBusy = value;
        }
        public Command GetVatCodesCommand
        {
            get { return _getVatCodesCommand ?? (_getVatCodesCommand = new Command(async () => await GetVatCodes())); }
        }
        private async Task GetVatCodes()
        {
            
            UserDialogs.Instance.ShowLoading("Proszę czekać, trwa logowanie...", MaskType.Gradient);
            var response = await _client.GetAsync("https://api2.wfirma.pl/vat_codes/find?outputFormat=xml");
            var generatedXml = await response.Content.ReadAsStringAsync();
            var objectResult = new VatCodesItem();
            if (generatedXml != "")
            {
                XmlSerializer serializer = new XmlSerializer(typeof(VatCodesItem));
                using (TextReader reader = new StringReader(generatedXml))
                {
                    objectResult = (VatCodesItem)serializer.Deserialize(reader);
                }
            }
            foreach (var vatCode in objectResult.VatCodes.Vat_code)
            {
                if (vatCode.Declaration_country.Id.Equals("0"))
                {
                    App.VatCodes.Add(vatCode);
                    App.VatCodesLabels.Add(vatCode.Label);
                    
                }
            }
            UserDialogs.Instance.HideLoading();
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
