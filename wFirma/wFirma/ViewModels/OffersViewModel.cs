﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml.Serialization;
using Acr.UserDialogs;
using ModernHttpClient;
using wFirma.Annotations;
using wFirma.Extensions;
using wFirma.Helpers;
using wFirma.Models;
using wFirma.Views;
using Xamarin.Forms;

namespace wFirma.ViewModels
{
    public class OffersViewModel : INotifyPropertyChanged
    {
        private readonly HttpClient _client;
        private string _title = string.Empty;
        private string _icon = null;
        private bool _isBusy;
        private bool _isNotBusy = true;
        private bool _isRefreshing = false;
        private Command _loadItemsCommand;
        private Command _deleteItemCommand;
        private Command _addItemCommand;
        private Command _editItemCommand;
        private Command _pdfPreviewCommand;
        private ICommand _refreshCommand;
        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<Invoice> _offers;
        private Invoice _offer;
        private Invoice _selectedOfferItem;
        public OffersViewModel()
        {
            var authData = string.Format("{0}:{1}", App.username, App.password);
            var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));
            _client = new HttpClient(new NativeMessageHandler());
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
            _offers = new ObservableCollection<Invoice>();
        }
        public string Title
        {
            get => _title;
            set => _title = value;
        }
        public string Icon
        {
            get => _icon;
            set => _icon = value;
        }
        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                if (_isBusy == value)
                {
                    return;
                }
                _isBusy = value;
                _isNotBusy = !_isBusy;
                OnPropertyChanged(nameof(IsBusy));
            }
        }
        public bool IsNotBusy
        {
            get => _isNotBusy;
            set => _isNotBusy = value;
        }
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                _isRefreshing = value;
                OnPropertyChanged(nameof(IsRefreshing));
            }
        }
        public ObservableCollection<Invoice> Offers
        {
            get => this._offers;
            set
            {
                if (_offers != value)
                {
                    _offers = value;
                    OnPropertyChanged(nameof(Offers));
                }
            }
        }
        public Invoice SelectedOfferItem
        {
            get { return _selectedOfferItem; }
            set
            {
                _selectedOfferItem = value;
                OnPropertyChanged();
            }
        }
        public Invoice GetOfferItem(string id)
        {
            return Offers.FirstOrDefault(x => x.Id.Equals(id));
        }
        public Invoice Offer
        {
            get { return _offer; }
            set
            {
                _offer = value;
                OnPropertyChanged();
            }
        }
        public ICommand RefreshCommand
        {
            get
            {
                return _refreshCommand ??
                       (_refreshCommand = new Command(async () =>
                       {
                           IsRefreshing = true;
                           await GetOffersData();
                           IsRefreshing = false;
                       }));
            }
        }
        public Command LoadItemsCommand
        {
            get
            {
                return _loadItemsCommand ?? (_loadItemsCommand =
                           new Command(async () =>
                           {
                               UserDialogs.Instance.ShowLoading("Proszę czekać...", MaskType.Gradient);
                               await GetOffersData();
                               UserDialogs.Instance.HideLoading();
                           }));
            }
        }

        public Command EditItemCommand
        {
            get { return _editItemCommand ?? (_editItemCommand = new Command(async (offerId) => await EditOfferData((String)offerId))); }
        }
        public Command RemoveItemCommand
        {
            get { return _deleteItemCommand ?? (_deleteItemCommand = new Command(async (offerId) => await RemoveOffer((String)offerId))); }
        }
//
        public Command AddItemCommand
        {
            get { return _addItemCommand ?? (_addItemCommand = new Command(async (offer) => await AddOffer((Invoice)offer))); }

        }
        public Command PdfPreviewCommand
        {
            get
            {
                return _pdfPreviewCommand ?? (_pdfPreviewCommand =
                           new Command(async (invoice) => await PDFPreview((Invoice)invoice)));
            }
        }
        public async Task GetOffersData()
        {
            if (_isBusy)
            {
                return;
            }
            _isBusy = true;
            var error = false;
            try
            {
                var response = await _client.GetAsync("https://api2.wfirma.pl/invoices/find?outputFormat=xml");
                var generatedXml = await response.Content.ReadAsStringAsync();
                var objectResult = new InvoiceItem();
                if (generatedXml != "")
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(InvoiceItem));
                    using (TextReader reader = new StringReader(generatedXml))
                    {
                        objectResult = (InvoiceItem)serializer.Deserialize(reader);
                    }
                }
                _offers.Clear();
                foreach (var offer in objectResult.Invoices.Invoice)
                {
                    if (offer.Type.Equals("offer"))
                    {
                        _offers.Add(offer);
                    }
                }
            }
            catch(Exception e)
            {
                error = true;
            }
            if (error)
            {
                var page = new ContentPage();
                var result = page.DisplayAlert("Błąd", "Wystąpił błąd podczas ładowania listy ofert.", "OK");
            }
            _isBusy = false;
        }

        public async Task EditOfferData(string offerId)
        {
            var error = false;
            UserDialogs.Instance.ShowLoading("Proszę czekać...", MaskType.Gradient);
            try
            {
                var editedOffer = GetOfferItem(offerId);
                InvoiceItem objectResult = new InvoiceItem();
                objectResult.Invoices = new Invoices();
                objectResult.Invoices.Invoice = new List<Invoice> {editedOffer};
                var xml = new XmlSerializer(typeof(ProductItem));
                var text = new Utf8StringWriter();
                xml.Serialize(text, objectResult);
                var content = new StringContent(text.ToString());
                var response = await _client.PostAsync("https://api2.wfirma.pl/goods/edit/" + offerId, content);
                var status = response.StatusCode;
                if (status == HttpStatusCode.OK)
                {
                    UserDialogs.Instance.Alert(new AlertConfig()
                        .SetMessage("Modyfikacja oferty przebiegła pomyślnie.")
                        .SetTitle("Sukces")
                        .SetOkText("OK"));
                }
            }
            catch
            {
                error = true;
            }
            if (error)
            {
                var page = new ContentPage();
                var result = page.DisplayAlert("Błąd", "Wystąpił błąd podczas modyfikacji oferty.", "OK");
            }
            UserDialogs.Instance.HideLoading();
        }

        public async Task RemoveOffer(string offerId)
        {
            var result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig()
                .SetMessage("Czy chcesz usunąć wybraną ofertę?")
                .SetTitle("Usuń ofertę")
                .SetCancelText("Nie")
                .SetOkText("Tak"));
            if (result)
            {
                UserDialogs.Instance.ShowLoading("Proszę czekać...", MaskType.Gradient);
                await Task.Delay(1000);
                _isBusy = true;
                var query = await _client.GetAsync("https://api2.wfirma.pl/invoices/delete/" + offerId+ "?outputFormat=xml");
                var generatedXml = await query.Content.ReadAsStringAsync();
                var objectResult = new InvoiceItem();
                if (generatedXml != null)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(InvoiceItem));
                    using (TextReader reader = new StringReader(generatedXml))
                    {
                        objectResult = (InvoiceItem)serializer.Deserialize(reader);
                    }
                }
                var product = GetOfferItem(offerId);
                Offers.Remove(product);
                switch (objectResult.Status.Code)
                {
                    case "NOT FOUND":
                        UserDialogs.Instance.Alert(new AlertConfig()
                            .SetMessage("Podany obiekt nie istnieje")
                            .SetTitle("Błąd")
                            .SetOkText("OK"));
                        break;
                    case "OK":
                        UserDialogs.Instance.Alert(new AlertConfig()
                            .SetMessage(objectResult.Status.Message)
                            .SetTitle("Sukces")
                            .SetOkText("OK"));
                        break;
                }
                _isBusy = false;
                UserDialogs.Instance.HideLoading();
            }
        }

        public async Task AddOffer(Invoice newOffer)
        {
            var error = false;
            UserDialogs.Instance.ShowLoading("Proszę czekać...", MaskType.Gradient);
            try
            {
                var objectResult = new InvoiceItem {Invoices = new Invoices {Invoice = new List<Invoice> {newOffer}}};
                var xml = new XmlSerializer(typeof(InvoiceItem));
                var text = new Utf8StringWriter();
                xml.Serialize(text, objectResult);
                var content = new StringContent(text.ToString());
                var response = await _client.PostAsync("https://api2.wfirma.pl/invoices/add/", content);
                var status = response.StatusCode;
                if (status == HttpStatusCode.OK)
                {
                    UserDialogs.Instance.Alert(new AlertConfig()
                        .SetMessage("Dodanie nowej oferty przebiegło pomyślnie")
                        .SetTitle("Sukces")
                        .SetOkText("OK"));
                }
                Offers.Add(newOffer);
                
            }
            catch
            {
                error = true;
            }
            if (error)
            {
                var page = new ContentPage();
                var result = page.DisplayAlert("Błąd", "Wystąpił błąd dodawania nowej oferty.", "OK");
            }
            UserDialogs.Instance.HideLoading();
        }
        private async Task PDFPreview(Invoice invoice)
        {
            var error = false;
            UserDialogs.Instance.ShowLoading("Proszę czekać...", MaskType.Gradient);
            try
            {
                var pdfParameters = new List<PdfParameter>();
                var firstParameter = new PdfParameter();
                firstParameter.Name = "page";
                firstParameter.Value = "invoicecopy";
                pdfParameters.Add(firstParameter);
                var secondParameter = new PdfParameter();
                secondParameter.Name = "address";
                secondParameter.Value = "0";
                pdfParameters.Add(secondParameter);
                var thirdParameter = new PdfParameter();
                thirdParameter.Name = "leaflet";
                thirdParameter.Value = "0";
                pdfParameters.Add(thirdParameter);
                var fourthParameter = new PdfParameter();
                fourthParameter.Name = "duplicate";
                fourthParameter.Value = "0";
                pdfParameters.Add(fourthParameter);
                PdfPreviewItem objectResult = new PdfPreviewItem
                {
                    Invoices = new PdfInvoices
                    {
                        Parameters = new PdfParameters
                        {
                            Parameter = pdfParameters
                        }
                    }
                };
                var xml = new XmlSerializer(typeof(PdfPreviewItem));
                var text = new Utf8StringWriter();
                xml.Serialize(text, objectResult);
                var content = new StringContent(text.ToString());
                var response = await _client.PostAsync("https://api2.wfirma.pl/invoices/download/" + invoice.Id, content);
                var status = response.StatusCode;
                var data = await _client.GetByteArrayAsync("https://api2.wfirma.pl/invoices/download/" + invoice.Id);
                string file = DependencyService.Get<ISaveAndLoad>().SaveText("Invoice.pdf", data);
                OfferPDFView.filename = file;
              
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                error = true;
            }
            if (error)
            {
                var page = new ContentPage();
                var result = page.DisplayAlert("Błąd", "Wystąpił błąd podczas podglądu faktury w PDF.", "OK");
            }
            UserDialogs.Instance.HideLoading();
        }
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        }
    }
}

