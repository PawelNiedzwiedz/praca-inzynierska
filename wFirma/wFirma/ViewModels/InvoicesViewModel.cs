﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml.Serialization;
using Acr.UserDialogs;
using ModernHttpClient;
using wFirma.Annotations;
using wFirma.Extensions;
using wFirma.Helpers;
using wFirma.Models;
using Xamarin.Forms;
using wFirma.Views;

namespace wFirma.ViewModels
{
    public class InvoicesViewModel : INotifyPropertyChanged
    {
        private HttpClient _client;
        private string _title = string.Empty;
        private string _icon = null;
        private bool _isBusy;
        private bool _isNotBusy = true;
        private bool _isRefreshing = false;
        private Command _loadItemsCommand;
        private Command _deleteItemCommand;
        private Command _addItemCommand;
        private Command _editItemCommand;
        private Command _payInvoiceCommand;
        private Command _pdfPreviewCommand;
        private ICommand _refreshCommand;
        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<Invoice> _invoices;
        private Invoice _invoice;
        private Invoice _selectedInvoiceItem;
        public InvoicesViewModel()
        {
            var authData = string.Format("{0}:{1}", App.username, App.password);
            var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));
            _client = new HttpClient(new NativeMessageHandler());
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
            _invoices = new ObservableCollection<Invoice>();
        }
        public string Title
        {
            get => _title;
            set => _title = value;
        }
        public string Icon
        {
            get => _icon;
            set => _icon = value;
        }
        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                if (_isBusy == value)
                {
                    return;
                }
                _isBusy = value;
                _isNotBusy = !_isBusy;
                OnPropertyChanged(nameof(IsBusy));
            }
        }
        public bool IsNotBusy
        {
            get => _isNotBusy;
            set => _isNotBusy = value;
        }
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                _isRefreshing = value;
                OnPropertyChanged(nameof(IsRefreshing));
            }
        }
        public ObservableCollection<Invoice> Invoices
        {
            get => this._invoices;
            set
            {
                if (_invoices != value)
                {
                    _invoices = value;
                    OnPropertyChanged(nameof(Invoices));
                }
            }
        }
        public Invoice SelectedInvoiceItem
        {
            get { return _selectedInvoiceItem; }
            set
            {
                _selectedInvoiceItem = value;
                OnPropertyChanged();
            }
        }
        public Invoice GetInvoiceItem(string id)
        {
            return Invoices.FirstOrDefault(x => x.Id.Equals(id));
        }
        public Invoice Invoice
        {
            get { return _invoice; }
            set
            {
                _invoice = value;
                OnPropertyChanged();
            }
        }
        public ICommand RefreshCommand
        {
            get
            {
                return _refreshCommand ??
                       (_refreshCommand = new Command(async () =>
                       {
                           IsRefreshing = true;
                           await GetInvoicesData();
                           IsRefreshing = false;
                       }));
            }
        }
        public Command LoadItemsCommand
        {
            get
            {
                return _loadItemsCommand ?? (_loadItemsCommand =
                           new Command(async () =>
                           {
                               UserDialogs.Instance.ShowLoading("Proszę czekać...", MaskType.Gradient);
                               await GetInvoicesData();
                               UserDialogs.Instance.HideLoading();
                           }));
            }
        }
        public Command EditItemCommand
        {
            get { return _editItemCommand ?? (_editItemCommand = new Command(async (invoiceId) => await EditInvoiceData((String)invoiceId))); }
        }
        public Command RemoveItemCommand
        {
            get { return _deleteItemCommand ?? (_deleteItemCommand = new Command(async (invoiceId) => await RemoveInvoice((String)invoiceId))); }
        }
        
        public Command AddItemCommand
        {
            get { return _addItemCommand ?? (_addItemCommand = new Command(async (offer) => await AddInvoice((Invoice)offer))); }

        }
        public Command PayInvoiceCommand
        {
            get
            {
                return _payInvoiceCommand ?? (_payInvoiceCommand = new Command(async(invoice) => await PayInvoice((Invoice)invoice)));
            }
        }

        public Command PdfPreviewCommand
        {
            get
            {
                return _pdfPreviewCommand ?? (_pdfPreviewCommand =
                           new Command(async (invoice) => await PDFPreview((Invoice) invoice)));
            }
        }
        private async Task GetInvoicesData()
        {
            if (_isBusy)
            {
                return;
            }
            _isBusy = true;
            var error = false;
            try
            {
                var response = await _client.GetAsync("https://api2.wfirma.pl/invoices/find?outputFormat=xml");
                var generatedXml = await response.Content.ReadAsStringAsync();
                var objectResult = new InvoiceItem();
                if (generatedXml != "")
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(InvoiceItem));
                    using (TextReader reader = new StringReader(generatedXml))
                    {
                        objectResult = (InvoiceItem)serializer.Deserialize(reader);
                    }
                }
                _invoices.Clear();
                foreach (var offer in objectResult.Invoices.Invoice)
                {
                    if (offer.Type.Equals("normal"))
                    {
                        _invoices.Add(offer);
                    }
                }
            }
            catch (Exception e)
            {
                error = true;
            }
            if (error)
            {
                var page = new ContentPage();
                var result = page.DisplayAlert("Błąd", "Wystąpił błąd podczas ładowania listy faktur.", "OK");
            }
            _isBusy = false;
        }
        private async Task EditInvoiceData(string invoiceId)
        {
            var error = false;
            UserDialogs.Instance.ShowLoading("Proszę czekać...", MaskType.Gradient);
            try
            {
                var editedInvoice = GetInvoiceItem(invoiceId);
                InvoiceItem objectResult = new InvoiceItem();
                objectResult.Invoices = new Invoices();
                objectResult.Invoices.Invoice = new List<Invoice> { editedInvoice };
                var xml = new XmlSerializer(typeof(InvoiceItem));
                var text = new Utf8StringWriter();
                xml.Serialize(text, objectResult);
                var content = new StringContent(text.ToString());
                var response = await _client.PostAsync("https://api2.wfirma.pl/invoices/edit/" + invoiceId, content);
                var status = response.StatusCode;
                if (status == HttpStatusCode.OK)
                {
                    UserDialogs.Instance.Alert(new AlertConfig()
                        .SetMessage("Modyfikacja faktury przebiegła pomyślnie.")
                        .SetTitle("Sukces")
                        .SetOkText("OK"));
                }
            }
            catch
            {
                error = true;
            }
            if (error)
            {
                var page = new ContentPage();
                var result = page.DisplayAlert("Błąd", "Wystąpił błąd podczas modyfikacji faktury.", "OK");
            }
            UserDialogs.Instance.HideLoading();
        }
        private async Task RemoveInvoice(string invoiceId)
        {
            var result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig()
                .SetMessage("Czy chcesz usunąć wybraną fakturę?")
                .SetTitle("Usuń fakturę")
                .SetCancelText("Nie")
                .SetOkText("Tak"));
            if (result)
            {
                UserDialogs.Instance.ShowLoading("Proszę czekać...", MaskType.Gradient);
                await Task.Delay(1000);
                _isBusy = true;
                var query = await _client.GetAsync("https://api2.wfirma.pl/invoices/delete/" + invoiceId + "?outputFormat=xml");
                var generatedXml = await query.Content.ReadAsStringAsync();
                var objectResult = new InvoiceItem();
                if (generatedXml != null)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(InvoiceItem));
                    using (TextReader reader = new StringReader(generatedXml))
                    {
                        objectResult = (InvoiceItem)serializer.Deserialize(reader);
                    }
                }
                var invoice = GetInvoiceItem(invoiceId);
                Invoices.Remove(invoice);
                switch (objectResult.Status.Code)
                {
                    case "NOT FOUND":
                        UserDialogs.Instance.Alert(new AlertConfig()
                            .SetMessage("Podany obiekt nie istnieje")
                            .SetTitle("Błąd")
                            .SetOkText("OK"));
                        break;
                    case "OK":
                        UserDialogs.Instance.Alert(new AlertConfig()
                            .SetMessage(objectResult.Status.Message)
                            .SetTitle("Sukces")
                            .SetOkText("OK"));
                        break;
                }
                _isBusy = false;
                UserDialogs.Instance.HideLoading();
            }
        }
        private async Task AddInvoice(Invoice newInvoice)
        {
            var error = false;
            UserDialogs.Instance.ShowLoading("Proszę czekać...", MaskType.Gradient);
            try
            {
                var objectResult =
                    new InvoiceItem { Invoices = new Invoices { Invoice = new List<Invoice> { newInvoice } } };
                var xml = new XmlSerializer(typeof(InvoiceItem));
                var text = new Utf8StringWriter();
                xml.Serialize(text, objectResult);
                var content = new StringContent(text.ToString());
                var response = await _client.PostAsync("https://api2.wfirma.pl/invoices/add/", content);
                var status = response.StatusCode;
                if (status == HttpStatusCode.OK)
                {
                    UserDialogs.Instance.Alert(new AlertConfig()
                        .SetMessage("Dodanie nowej faktury przebiegło pomyślnie")
                        .SetTitle("Sukces")
                        .SetOkText("OK"));
                }
                Invoices.Add(newInvoice);

            }
            catch
            {
                error = true;
            }
            if (error)
            {
                var page = new ContentPage();
                var result = page.DisplayAlert("Błąd", "Wystąpił błąd dodawania nowej faktury.", "OK");
            }
            UserDialogs.Instance.HideLoading();
        }
        private async Task PayInvoice(Invoice invoice)
        {
            if (invoice.Remaining != "0.00")
            {
                var result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig()
                    .SetMessage("Czy chcesz rozliczyć fakturę " + invoice.Fullnumber + " wystawioną dla " +
                                invoice.ContractorDetail.Name + " na kwotę " + invoice.Remaining + "zł?")
                    .SetTitle("Rozliczanie płatności")
                    .SetCancelText("Nie")
                    .SetOkText("Tak"));
                if (result)
                {
                    var error = false;
                    try
                    {
                        UserDialogs.Instance.ShowLoading("Proszę czekać...", MaskType.Gradient);
                        await Task.Delay(1000);
                        _isBusy = true;
                        var payment = new Payment();
                        payment.Object_name = "invoice";
                        payment.Object_id = invoice.Id;
                        payment.Value = invoice.Vat_contents.Vat_content.Brutto;
                        payment.Date = DateTime.Now.Date.ToString("yyyy-MM-dd");
                        var objectResult = new PaymentItem {Payments = new Payments {Payment = new List<Payment>()}};
                        objectResult.Payments.Payment.Add(payment);
                        var xml = new XmlSerializer(typeof(PaymentItem));
                        var text = new Utf8StringWriter();
                        xml.Serialize(text, objectResult);
                        var content = new StringContent(text.ToString());
                        var response = await _client.PostAsync("https://api2.wfirma.pl/payments/add/", content);
                        _isBusy = false;
                        UserDialogs.Instance.HideLoading();
                        var status = response.StatusCode;
                        if (status == HttpStatusCode.OK)
                        {
                            invoice.Paymentstate = "paid";
                            invoice.Alreadypaid = invoice.Vat_contents.Vat_content.Brutto;
                            invoice.Remaining = "0.00";
                            InvoiceItem objectInvoiceItem = new InvoiceItem();
                            objectInvoiceItem.Invoices = new Invoices();
                            objectInvoiceItem.Invoices.Invoice = new List<Invoice> {invoice};
                            var generatedXml = new XmlSerializer(typeof(InvoiceItem));
                            var generatedText = new Utf8StringWriter();
                            xml.Serialize(text, objectResult);
                            var invoiceContent = new StringContent(text.ToString());
                            var responseMessage =
                                await _client.PostAsync("https://api2.wfirma.pl/invoices/edit/" + invoice.Id,
                                    invoiceContent);
                            _isBusy = false;
                            UserDialogs.Instance.HideLoading();
                            var invoiceStatusCode = response.StatusCode;
                            if (invoiceStatusCode == HttpStatusCode.OK)
                            {
                                UserDialogs.Instance.Alert(new AlertConfig()
                                    .SetMessage("Wybrana faktura została rozliczona.")
                                    .SetTitle("Sukces")
                                    .SetOkText("OK"));
                            }

                        }
                    }
                    catch
                    {
                        error = true;
                    }
                    if (error)
                    {
                        var page = new ContentPage();
                        var resultError = page.DisplayAlert("Błąd", "Wystąpił błąd podczas rozliczania faktury.", "OK");
                    }
                }
            }
            else
            {
                await UserDialogs.Instance.AlertAsync(new AlertConfig()
                    .SetMessage("Wybrana faktura " + invoice.Fullnumber + " wystawiona dla " +
                                invoice.ContractorDetail.Name + " na kwotę " + invoice.Total + "zł została już rozliczona.")
                    .SetTitle("Rozliczanie płatności")
                    .SetOkText("Zatwierdź"));
            }
            
        }
        private async Task PDFPreview(Invoice invoice)
        {
            var error = false;
            UserDialogs.Instance.ShowLoading("Proszę czekać...", MaskType.Gradient);
            try
            {
                var pdfParameters = new List<PdfParameter>();
                var firstParameter = new PdfParameter();
                firstParameter.Name = "page";
                firstParameter.Value = "invoicecopy";
                pdfParameters.Add(firstParameter);
                var secondParameter = new PdfParameter();
                secondParameter.Name = "address";
                secondParameter.Value = "0";
                pdfParameters.Add(secondParameter);
                var thirdParameter = new PdfParameter();
                thirdParameter.Name = "leaflet";
                thirdParameter.Value = "0";
                pdfParameters.Add(thirdParameter);
                var fourthParameter = new PdfParameter();
                fourthParameter.Name = "duplicate";
                fourthParameter.Value = "0";
                pdfParameters.Add(fourthParameter);
                PdfPreviewItem objectResult = new PdfPreviewItem
                {
                    Invoices = new PdfInvoices
                    {
                        Parameters = new PdfParameters {Parameter = pdfParameters
                        }
                    }
                };
                var xml = new XmlSerializer(typeof(PdfPreviewItem));
                var text = new Utf8StringWriter();
                xml.Serialize(text, objectResult);
                var content = new StringContent(text.ToString());
                var response = await _client.PostAsync("https://api2.wfirma.pl/invoices/download/" + invoice.Id, content);
                var status = response.StatusCode;
                var data = await _client.GetByteArrayAsync("https://api2.wfirma.pl/invoices/download/" + invoice.Id);
                /*string Name = @"C:\temp\yourfile.pdf";
                MemoryStream ms = new MemoryStream();//data, true);
                ms.
                BinaryWriter writer = new System.IO.BinaryWriter(ms, Encoding.UTF8);
                writer.Write(data);*/
                /*DependencyService.Get<ISaveAndLoad>().SaveText("temp.txt", input.Text);
                System.IO.File.WriteAllBytes
                using (var streamWriter = new StreamWriter(filename, true))
                {
                    streamWriter.WriteLine(DateTime.UtcNow);
                }*/
                string file = DependencyService.Get<ISaveAndLoad>().SaveText("Invoice.pdf", data);
                InvoicePDFView.filename = file;
                //                if (status == HttpStatusCode.OK)
                //                {
                //                    UserDialogs.Instance.Alert(new AlertConfig()
                //                        .SetMessage("Modyfikacja faktury przebiegła pomyślnie.")
                //                        .SetTitle("Sukces")
                //                        .SetOkText("OK"));
                //                }
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
                error = true;
            }
            if (error)
            {
                var page = new ContentPage();
                var result = page.DisplayAlert("Błąd", "Wystąpił błąd podczas podglądu faktury w PDF.", "OK");
            }
            UserDialogs.Instance.HideLoading();
        }



        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
