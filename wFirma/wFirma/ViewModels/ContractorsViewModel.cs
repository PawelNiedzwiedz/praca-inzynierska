﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml.Serialization;
using Acr.UserDialogs;
using ModernHttpClient;
using wFirma.Annotations;
using wFirma.Extensions;
using wFirma.Models;
using Xamarin.Forms;

namespace wFirma.ViewModels
{
    public class ContractorsViewModel : INotifyPropertyChanged
    {
        
        private HttpClient _client;
        private string _title = string.Empty;
        private string _icon = null;
        private bool _isBusy;
        private bool _isNotBusy = true;
        private bool _isRefreshing = false;
        private Command _loadItemsCommand;
        private Command _deleteItemCommand;
        private Command _addItemCommand;
        private Command _editItemCommand;
        private ICommand _refreshCommand;
        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<Contractor> _contractors;
        private Contractor _contractor;
        private Contractor _selectedContractorItem;
        public ContractorsViewModel()
        {
            var authData = string.Format("{0}:{1}", App.username, App.password);
            var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));
            _client = new HttpClient(new NativeMessageHandler());
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
            _contractors = new ObservableCollection<Contractor>();
        }

        public string Title
        {
            get => _title;
            set => _title = value;
        }
        public string Icon
        {
            get => _icon;
            set => _icon = value;
        }
        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                if (_isBusy == value)
                {
                    return;
                }
                _isBusy = value;
                _isNotBusy = !_isBusy;
                OnPropertyChanged(nameof(IsBusy));
            }
        }
        public bool IsNotBusy
        {
            get => _isNotBusy;
            set => _isNotBusy = value;
        }
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                _isRefreshing = value;
                OnPropertyChanged(nameof(IsRefreshing));
            }
        }
        public ObservableCollection<Contractor> Contractors
        {
            get => this._contractors;
            set
            {
                if (_contractors != value)
                {
                    _contractors = value;
                    OnPropertyChanged(nameof(Contractors));
                }
            }
        }
        public Contractor SelectedContractorItem
        {
            get { return _selectedContractorItem; }
            set
            {
                _selectedContractorItem = value;               
                OnPropertyChanged();
            }
        }
        public Contractor GetContractorItem(string id)
        {
            return Contractors.FirstOrDefault(x => x.Id.Equals(id));
        }
        public Contractor Contractor
        {
            get { return _contractor; }
            set
            {
                _contractor = value;
                OnPropertyChanged();
            }
        }
        public Command LoadItemsCommand
        {
            get
            {
                return _loadItemsCommand ?? (_loadItemsCommand =
                           new Command(async () =>
                           {
                               UserDialogs.Instance.ShowLoading("Proszę czekać...", MaskType.Gradient);
                               await GetContractorsData();
                               UserDialogs.Instance.HideLoading();
                           }));
            }
        }
        public Command RemoveItemCommand
        {
            get { return _deleteItemCommand ?? (_deleteItemCommand = new Command(async (contractorId) => await RemoveContractor((String)contractorId))); }
        }
        public Command AddItemCommand
        {
            get { return _addItemCommand ?? (_addItemCommand = new Command(async (contractor) => await AddContractor((Contractor)contractor))); }
        }
        public Command EditItemCommand
        {
            get { return _editItemCommand ?? (_editItemCommand = new Command(async (contractorId) => await EditContractorData((String)contractorId)));}
        }
        public ICommand RefreshCommand
        {
            get
            {
                return _refreshCommand ??
                       (_refreshCommand = new Command(async () =>
                       {
                           IsRefreshing = true;
                           await GetContractorsData();
                           IsRefreshing = false;
                       }));
            }
        }
        private async Task GetContractorsData()
        {
            if (_isBusy)
            {
                return;
            }
            _isBusy = true;
            var error = false;
            try
            {
//                //Body
//                var vatCode = new Vat_code();
//                var newVatCodeItem = new VatCodesItem();
//                newVatCodeItem.VatCodes = new VatCodes();
//                newVatCodeItem.VatCodes.Parameters = new VatCodesParameters();
//                newVatCodeItem.VatCodes.Parameters.Limit = "70";
//                newVatCodeItem.VatCodes.Vat_code = new List<Vat_code>();
//                newVatCodeItem.VatCodes.Vat_code.Add(vatCode);
//                XmlSerializer xml = new XmlSerializer(typeof(VatCodesItem));
//                Utf8StringWriter text = new Utf8StringWriter();
//                xml.Serialize(text, newVatCodeItem);
//                var content = new StringContent(text.ToString());
//                var response = await _client.PostAsync("https://api2.wfirma.pl/vat_codes/find/", content);
//                var status = response.StatusCode;
//
//                var response2 = await _client.GetAsync("https://api2.wfirma.pl/vat_codes/find?outputFormat=xml");
//                var generatedXml = await response.Content.ReadAsStringAsync();
//                var objectResult = new VatCodesItem();
//                if (generatedXml != "")
//                {
//                    XmlSerializer serializer = new XmlSerializer(typeof(VatCodesItem));
//                    using (TextReader reader = new StringReader(generatedXml))
//                    {
//                        objectResult = (VatCodesItem) serializer.Deserialize(reader);
//                    }
//                }
                var response = await _client.GetAsync("https://api2.wfirma.pl/contractors/find?outputFormat=xml");
                var generatedXml = await response.Content.ReadAsStringAsync();
                var objectResult = new ContractorItem();
                if (generatedXml != "")
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ContractorItem));
                    using (TextReader reader = new StringReader(generatedXml))
                    {
                        objectResult = (ContractorItem) serializer.Deserialize(reader);
                    }
                }
                _contractors.Clear();
                foreach (var contractor in objectResult.Contractors.Contractor)
                {
                    if (!_contractors.Contains(contractor))
                    {
                        _contractors.Add(contractor);
                    }
                }
            }
            catch
            {                 
                error = true;
            }
            if (error)
            {
                var page = new ContentPage();
                var result = page.DisplayAlert("Błąd", "Wystąpił błąd podczas ładowania listy kontrahentów.", "OK");
            }
            _isBusy = false;
        }
        private async Task EditContractorData(string contractorId)
        {
            var error = false;
            UserDialogs.Instance.ShowLoading("Proszę czekać...", MaskType.Gradient);
            try
            {
                var editedContractor = GetContractorItem(contractorId);
                ContractorItem objectResult = new ContractorItem();
                objectResult.Contractors = new Contractors();
                objectResult.Contractors.Contractor = new List<Contractor>();
                objectResult.Contractors.Contractor.Add(editedContractor);
                XmlSerializer xml = new XmlSerializer(typeof(ContractorItem));
                Utf8StringWriter text = new Utf8StringWriter();
                xml.Serialize(text, objectResult);
                var content = new StringContent(text.ToString());
                var response = await _client.PostAsync("https://api2.wfirma.pl/contractors/edit/" + contractorId, content);
                var message = response.RequestMessage;
                var status = response.StatusCode;
                if (status == HttpStatusCode.OK)
                {
                    UserDialogs.Instance.Alert(new AlertConfig()
                        .SetMessage("Modyfikacja kontrahenta przebiegła pomyślnie")
                        .SetTitle("Sukces")
                        .SetOkText("OK"));
                }
            }
            catch
            {
                error = true;
            }
            if (error)
            {
                var page = new ContentPage();
                var result = page.DisplayAlert("Błąd", "Wystąpił błąd modyfikowania kontrahenta.", "OK");
            }
            UserDialogs.Instance.HideLoading();
        }
        private async Task RemoveContractor(string contractorId)
        {
            var result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig()
                .SetMessage("Czy chcesz usunąć wybranego kontrahenta?")
                .SetTitle("Usuń kontrahenta")
                .SetCancelText("Nie")
                .SetOkText("Tak"));
            if (result)
            {
                UserDialogs.Instance.ShowLoading("Proszę czekać...", MaskType.Gradient);
                await Task.Delay(1000);
                _isBusy = true;
                var query = await _client.GetAsync("https://api2.wfirma.pl/contractors/delete/"+contractorId+"?outputFormat=xml");
                var generatedXml = await query.Content.ReadAsStringAsync();
                var objectResult = new ContractorItem();
                if (generatedXml != null)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ContractorItem));
                    using (TextReader reader = new StringReader(generatedXml))
                    {
                        objectResult = (ContractorItem)serializer.Deserialize(reader);
                    }
                }
                var contractor = GetContractorItem(contractorId);
                Contractors.Remove(contractor);
                switch (objectResult.Status.Code)
                {
                    case "NOT FOUND":
                        UserDialogs.Instance.Alert(new AlertConfig()
                            .SetMessage("Podany obiekt nie istnieje")
                            .SetTitle("Błąd")
                            .SetOkText("OK"));
                        break;
                    case "OK":
                        UserDialogs.Instance.Alert(new AlertConfig()
                            .SetMessage(objectResult.Status.Message)
                            .SetTitle("Sukces")
                            .SetOkText("OK"));
                        break;
                }
                _isBusy = false;
                UserDialogs.Instance.HideLoading();
            }
        }
        private async Task AddContractor(Contractor newContractor)
        {
            var error = false;
            UserDialogs.Instance.ShowLoading("Proszę czekać...", MaskType.Gradient);
            try
            {
                ContractorItem objectResult = new ContractorItem();
                objectResult.Contractors = new Contractors();
                objectResult.Contractors.Contractor = new List<Contractor>();
                objectResult.Contractors.Contractor.Add(newContractor);
                XmlSerializer xml = new XmlSerializer(typeof(ContractorItem));
                Utf8StringWriter text = new Utf8StringWriter();
                xml.Serialize(text, objectResult);
                var content = new StringContent(text.ToString());
                var response = await _client.PostAsync("https://api2.wfirma.pl/contractors/add/", content);
                var status = response.StatusCode;
                if (status == HttpStatusCode.OK)
                {
                    UserDialogs.Instance.Alert(new AlertConfig()
                        .SetMessage("Dodanie nowego kontrahenta przebiegło pomyślnie")
                        .SetTitle("Sukces")
                        .SetOkText("OK"));
                }
                Contractors.Add(newContractor);
            }
            catch
            {
                error = true;
            }
            if (error)
            {
                var page = new ContentPage();
                var result = page.DisplayAlert("Błąd", "Wystąpił błąd modyfikowania kontrahenta.", "OK");
            }
            UserDialogs.Instance.HideLoading();
        }
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
