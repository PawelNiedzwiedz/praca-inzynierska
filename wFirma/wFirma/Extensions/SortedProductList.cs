﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using wFirma.Models;

namespace wFirma.Extensions
{
    public class SortedProductList : ObservableCollection<Good>
    {
        public string Title { get; set; }
        public string ShortTitle { get; set; }
    }
}
