﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wFirma.Helpers
{
    public interface ISaveAndLoad
    {
        string SaveText(string filename, byte[] text);
        string LoadText(string filename);
    }
}
