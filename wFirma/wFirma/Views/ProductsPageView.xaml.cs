﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Syncfusion.ListView.XForms;
using wFirma.Models;
using wFirma.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace wFirma.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductsPageView : ContentPage
    {
        public object SelectedProduct = null;

        private ProductsViewModel ViewModel
        {
            get { return BindingContext as ProductsViewModel; }
        }
        public ProductsPageView(bool selecting)
        {
            InitializeComponent();
            BindingContext = new ProductsViewModel()
            {
                Title = "Produkty",
                Icon = "products.png",
                IsBusy = false,
                IsNotBusy = true
            };
           
            ProductsListView.ItemTapped += (sender, args) =>
            {
                if (ProductsListView.SelectedItem == null)
                    return;
                Good item = (Good)args.Item;
                ViewModel.SelectedProductItem = item;
                if (selecting)
                {
                    SelectedProduct = (Invoicecontent)item;
                    Navigation.PopAsync(true);
                    return;
                }
                UserDialogs.Instance.ActionSheet(new ActionSheetConfig().SetTitle("Wybierz jedną z opcji:")
                    .Add("Wyświetl produkt", () => Navigation.PushAsync(new ProductView(item, "", ViewModel)))
                    .Add("Modyfikuj produkt", () => Navigation.PushModalAsync(new NavigationPage(new ProductView(item, "edit", ViewModel))))
                    .SetDestructive("Usuń produkt", () => ViewModel.RemoveItemCommand.Execute(ViewModel.SelectedProductItem.Id))
                    .SetCancel("Anuluj")
                    .SetUseBottomSheet(true));
                ProductsListView.SelectedItem = null;
            };
            var addNewProduct = new ToolbarItem
            {
                Text = "Nowy produkt",
                Command = new Command(() =>
                {
                    Good item = new Good();
                    Navigation.PushModalAsync(new NavigationPage(new ProductView(item, "add", ViewModel)));
                }),
                Icon = "addIcon2.png"
            };
            ToolbarItems.Add(addNewProduct);
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (ViewModel == null || ViewModel.IsBusy || ViewModel.Products.Count > 0)
                return;

            ViewModel.LoadItemsCommand.Execute(null);
        }
        protected override bool OnBackButtonPressed()
        {
            return true;
        }
    }
}