﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using wFirma.Models;
using wFirma.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace wFirma.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductView : ContentPage
    {
        private bool canGoBack = false;
        public ProductView(Good product, string scenario, ProductsViewModel context)
        {
            InitializeComponent();
            product.IsEditable = scenario.Equals("edit") || scenario.Equals("add");
            var backupProduct = JsonConvert.DeserializeObject<Good>(JsonConvert.SerializeObject(product));
            BindingContext = product;
            var accept = new ToolbarItem
            {
                Text = "Zatwierdź",
                Command = new Command(() =>
                {
                    switch (scenario)
                    {
                        case "edit":
                            context?.EditItemCommand.Execute(product.Id);
                            break;
                        case "add":
                            context?.AddItemCommand.Execute(product);
                            break;
                    }
                    Navigation.PopModalAsync(true);
                })
            };
            var cancel = new ToolbarItem
            {
                Text = "Anuluj",
                Priority = 2,
                Command = new Command(async () =>
                {
                    var result = false;
                    if (scenario.Equals("edit"))
                    {
                        result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig()
                            .SetMessage("Czy chcesz przerwać edycję wybranego produktu?")
                            .SetTitle("Przerwij edycję")
                            .SetCancelText("Nie")
                            .SetOkText("Tak"));
                    }
                    else if (scenario.Equals("add"))
                    {
                        result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig()
                            .SetMessage("Czy chcesz przerwać dodawanie nowego produktu?")
                            .SetTitle("Przerwij dodawanie")
                            .SetCancelText("Nie")
                            .SetOkText("Tak"));
                    }
                    if (result)
                    {

                        if (context != null)
                        {
                            Good editedProduct = context.Products.Where(x => x.Id == backupProduct.Id).FirstOrDefault();
                            if (editedProduct != null)
                            {
                                int indexOf = context.Products.IndexOf(editedProduct);
                                context.Products.RemoveAt(indexOf);
                                context.Products.Insert(indexOf, backupProduct);
                                canGoBack = true;
                            }
                        }
                        await Navigation.PopModalAsync(true);
                    }
                }),
            };
            switch (scenario)
            {
                case "edit":
                    Title = "Edytuj dane";
                    ToolbarItems.Add(accept);
                    ToolbarItems.Add(cancel);
                    break;
                case "add":
                    Title = "Nowy produkt";
                    ToolbarItems.Add(accept);
                    ToolbarItems.Add(cancel);
                    break;
                case "":
                    canGoBack = true;
                    break;
            }
        }

        private void DifferentTypeProduct(object sender, ToggledEventArgs e)
        {
            Count.IsVisible = !e.Value;
            CountLine.IsVisible = !e.Value;
            ProductType.Text = e.Value ? "Usługa" : "Towar";
        }

        private void Discount_OnToggled(object sender, ToggledEventArgs e)
        {
            DiscountValue.Text = e.Value ? "Tak" : "Nie";
        }

        protected override bool OnBackButtonPressed()
        {
            if (canGoBack)
            {
                return base.OnBackButtonPressed();
            }
            Device.BeginInvokeOnMainThread(() =>
            {
                ToolbarItems.ElementAt(1).Command.Execute(null);
            });
            return true;
        }
    }
}