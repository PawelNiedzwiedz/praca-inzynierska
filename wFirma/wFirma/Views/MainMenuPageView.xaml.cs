﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using wFirma.Models;
using wFirma.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace wFirma.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainMenuPageView : ContentPage
    {
        private RootPage rootPage;
        private List<MainMenuItem> mainMenuItems;
        public MainMenuPageView(RootPage root)
        {
            this.rootPage = root;
            InitializeComponent();
            BindingContext = new MenuViewModel
            {
                Title = "Menu główne",
                Icon = "hamburgermenu2.png",
                IsBusy = false,
                IsNotBusy = true
            };

            ListViewMenu.ItemsSource = mainMenuItems = new List<MainMenuItem>
            {
                new MainMenuItem{Id = 0, Title = "Strona główna", MainMenuType = MainMenuType.HomePage, Icon = "homepage.png"},
                new MainMenuItem{Id = 1, Title = "Kontrahenci", MainMenuType = MainMenuType.Contractors, Icon = "contractors.png"},
                new MainMenuItem{Id = 2, Title = "Produkty", MainMenuType = MainMenuType.Products, Icon = "products.png"},
                new MainMenuItem{Id = 3, Title = "Faktury", MainMenuType = MainMenuType.Invoices, Icon = "invoices.png"},
                new MainMenuItem{Id = 4, Title = "Oferty", MainMenuType = MainMenuType.Offers, Icon = "offers.png"},
            };
            ListViewMenu.SelectedItem = mainMenuItems[0];
            ListViewMenu.ItemSelected += ListViewMenu_ItemSelected;
        }

        private async void ListViewMenu_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (ListViewMenu.SelectedItem == null)
            {
                return;
            }
            await this.rootPage.NavigateAsync(((MainMenuItem)e.SelectedItem).MainMenuType);
        }

        private void TapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            rootPage.IsPresented = false;
            rootPage.ShowLoginPage(true);
        }
    }
}