﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using wFirma.Models;
using wFirma.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace wFirma.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InvoicePDFView : ContentPage
    {
        public static string filename;
        public InvoicePDFView(Invoice invoice, InvoicesViewModel context)
        {
            InitializeComponent();
            context.PdfPreviewCommand.Execute(invoice);
            pdfViewer.Uri = filename;
        }

    }
}