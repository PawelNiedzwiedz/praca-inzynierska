﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using wFirma.Models;
using wFirma.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Acr.UserDialogs;

namespace wFirma.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContractorsPageView : ContentPage
    {
        public object SelectedContractor = null;
        public ContractorsViewModel ViewModel
        {
            get { return BindingContext as ContractorsViewModel; }
        }
        public ContractorsPageView(bool selecting)
        {
            InitializeComponent();
            BindingContext = new ContractorsViewModel()
            {
                Title = "Kontrahenci",
                Icon = "contractors.png",
                IsBusy = false,
                IsNotBusy = true
            };
            
            ContractorsListView.ItemTapped += (sender, args) =>
            {
                if (ContractorsListView.SelectedItem == null)
                    return;
                Contractor item = (Contractor)args.Item;
                ViewModel.SelectedContractorItem = item;
                if (selecting)
                {
                    SelectedContractor = (ContractorDetail)item;
                    Navigation.PopAsync(true);
                    return;
                }
                
                UserDialogs.Instance.ActionSheet(new ActionSheetConfig().SetTitle("Wybierz jedną z opcji:")
                    .Add("Wyświetl kontrahenta", () => Navigation.PushAsync(new ContractorDetailView(item, "", ViewModel)))
                    .Add("Modyfikuj kontrahenta", () => Navigation.PushModalAsync(new NavigationPage(new ContractorDetailView(item, "edit", ViewModel))))
                    .SetDestructive("Usuń kontrahenta", () => ViewModel.RemoveItemCommand.Execute(ViewModel.SelectedContractorItem.Id))
                    .SetCancel("Anuluj")
                    .SetUseBottomSheet(true));
                ContractorsListView.SelectedItem = null;
            };
            var addNewContractor = new ToolbarItem
            {
                Text = "Nowy",
                Command = new Command(() =>
                {
                    Contractor item = new Contractor();
                    Navigation.PushModalAsync(new NavigationPage(new ContractorDetailView(item, "add", ViewModel)));
                }),
                Icon = "addIcon2.png",

            };
            ToolbarItems.Add(addNewContractor);
            
        }
       
        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (ViewModel == null || ViewModel.IsBusy || ViewModel.Contractors.Count > 0)
                return;
            ViewModel.LoadItemsCommand.Execute(null);
        }
        protected override bool OnBackButtonPressed()
        {
            return true;
        }
    }
}