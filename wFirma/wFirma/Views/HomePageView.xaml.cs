﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using wFirma.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace wFirma.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePageView : ContentPage
    {
        public HomePageViewModel ViewModel
        {
            get { return BindingContext as HomePageViewModel; }
        }
        public HomePageView()
        {
            InitializeComponent();
            BindingContext = new HomePageViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (ViewModel == null || ViewModel.IsBusy || App.VatCodes.Count>0)
                return;
            //ViewModel.GetVatCodesCommand.Execute(null);
        }
        protected override bool OnBackButtonPressed()
        {
            return true;
        }
    }
}