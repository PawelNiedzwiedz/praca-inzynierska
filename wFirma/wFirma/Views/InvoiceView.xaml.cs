﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using wFirma.Models;
using wFirma.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace wFirma.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InvoiceView : ContentPage
    {
        public InvoiceView(Invoice invoice)
        {
            InitializeComponent();
            foreach (var item in invoice.InvoiceContents.Invoicecontent)
            {
                var count = (int)Convert.ToDouble(item.Count.Replace(".", ","));
                item.Count = count.ToString();
            }
            BindingContext = invoice;
            var pay = new ToolbarItem
            {
                Text = "Rozlicz", 
                Command = new Command(() =>
                {
                    var context = Navigation.NavigationStack[0].BindingContext as InvoicesViewModel;
                    context?.PayInvoiceCommand.Execute(invoice);
                })
            };
            ToolbarItems.Add(pay);
        }
    }
}