﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using wFirma.Models;
using wFirma.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace wFirma.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InvoicesPageView : ContentPage
    {
        public InvoicesViewModel ViewModel
        {
            get { return BindingContext as InvoicesViewModel; }
        }
        public InvoicesPageView()
        {
            InitializeComponent();
            BindingContext = new InvoicesViewModel()
            {
                Title = "Faktury",
                Icon = "invoices.png",
                IsBusy = false,
                IsNotBusy = true
            };
            InvoicesListView.ItemTapped += (sender, args) =>
            {
                if (InvoicesListView.SelectedItem == null)
                    return;
                Invoice item = (Invoice)args.Item;
                ViewModel.SelectedInvoiceItem = item;
                UserDialogs.Instance.ActionSheet(new ActionSheetConfig().SetTitle("Wybierz jedną z opcji:")
                    .Add("Wyświetl fakturę", () => Navigation.PushAsync(new InvoiceView(item)))
                    .Add("Modyfikuj fakturę", () => Navigation.PushModalAsync(new NavigationPage(new InvoiceEditView(item, true, ViewModel))))
                    .Add("Podgląd PDF", () => Navigation.PushAsync(new InvoicePDFView(ViewModel.SelectedInvoiceItem, ViewModel)))
                    .SetDestructive("Usuń fakturę", () => ViewModel.RemoveItemCommand.Execute(ViewModel.SelectedInvoiceItem.Id))
                    .SetCancel("Anuluj")
                    .SetUseBottomSheet(true));
                InvoicesListView.SelectedItem = null;
            };

            var newItem = new ToolbarItem
            {
                Text = "Nowa faktura",
                Command = new Command(() =>
                {
                    var item = new Invoice();
                    Navigation.PushModalAsync(new NavigationPage(new InvoiceEditView(item, false, ViewModel)));
                }),
                Icon = "addIcon2.png"
            };
            ToolbarItems.Add(newItem);
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (ViewModel == null || ViewModel.IsBusy || ViewModel.Invoices.Count > 0)
                return;
            ViewModel.LoadItemsCommand.Execute(null);
        }

        private void PullToRefresh_OnRefreshing(object sender, EventArgs e)
        {
            ViewModel.RefreshCommand.Execute(null);
        }
        protected override bool OnBackButtonPressed()
        {
            return true;
        }
    }
}