﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using wFirma.Models;
using wFirma.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace wFirma.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class OfferView : ContentPage
	{
		public OfferView (Invoice offer)
		{
			InitializeComponent();
		    foreach (var item in offer.InvoiceContents.Invoicecontent)
		    {
		        var count = (int)Convert.ToDouble(item.Count.Replace(".", ","));
		        item.Count = count.ToString();
		    }
            BindingContext = offer;
        }
    }
}