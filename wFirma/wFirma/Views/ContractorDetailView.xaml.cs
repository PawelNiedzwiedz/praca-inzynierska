﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using wFirma.CustomControls;
using wFirma.Extensions;
using wFirma.Models;
using wFirma.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.Xaml;

namespace wFirma.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContractorDetailView : ContentPage
    {
        private bool canGoBack = false;
        public ContractorDetailView(Contractor contractor, string scenario, ContractorsViewModel context)
        {
            InitializeComponent();
            if (scenario.Equals("edit") || scenario.Equals("add"))
            {
                contractor.IsEditable = true;
            }
            else
            {
                contractor.IsEditable = false;
            }
            var backupContractor = JsonConvert.DeserializeObject<Contractor>(JsonConvert.SerializeObject(contractor));
            BindingContext = contractor;
            var accept = new ToolbarItem
            {
                Text = "Zatwierdź",
                Priority = 2,
                Command = new Command(() =>
                {
                    switch (scenario)
                    {
                        case "edit":
                            context?.EditItemCommand.Execute(contractor.Id);
                            break;
                        case "add":
                            context?.AddItemCommand.Execute(contractor);
                            break;
                    }
                    Navigation.PopModalAsync(true);
                })
            };
            var cancel = new ToolbarItem
            {
                Text = "Anuluj",
                Priority = 2,
                Command = new Command(async () =>
                {
                    var result = false;
                    if (scenario.Equals("edit"))
                    {
                        result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig()
                            .SetMessage("Czy chcesz przerwać edycję wybranego kontrahenta?")
                            .SetTitle("Przerwij edycję")
                            .SetCancelText("Nie")
                            .SetOkText("Tak"));
                    }
                    else if (scenario.Equals("add"))
                    {
                        result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig()
                            .SetMessage("Czy chcesz przerwać dodawanie nowego kontrahenta?")
                            .SetTitle("Przerwij dodawanie")
                            .SetCancelText("Nie")
                            .SetOkText("Tak"));
                    }
                    if (result)
                    {
                        
                        if (context != null)
                        {
                            Contractor editedContactor = context.Contractors.Where(x => x.Id == backupContractor.Id).FirstOrDefault();
                            if (editedContactor != null)
                            {
                                int indexOf = context.Contractors.IndexOf(editedContactor);
                                context.Contractors.RemoveAt(indexOf);
                                context.Contractors.Insert(indexOf, backupContractor);
                                canGoBack = true;
                            }
                        }
                        await Navigation.PopModalAsync(true);
                    }
                }),
            };
            switch (scenario)
            {
                case "edit":
                    Title = "Edytuj dane";
                    ToolbarItems.Add(accept);
                    ToolbarItems.Add(cancel);
                    break;
                case "add":
                    Title = "Nowy kontrahent";
                    ToolbarItems.Add(accept);
                    ToolbarItems.Add(cancel);
                    break;
                case "":
                    canGoBack = true;
                    break;
            }

        }
        private void DifferentContactAddressSwitch_OnToggled(object sender, ToggledEventArgs e)
        {
            DifferentContactAddressLayout.IsVisible = e.Value;
            DifferentContactAddressText.Text = e.Value ? "Tak" : "Nie";
        }
        protected override bool OnBackButtonPressed()
        {
            if (canGoBack)
            {
                return base.OnBackButtonPressed();
            }
            Device.BeginInvokeOnMainThread(() =>
            {
                ToolbarItems.ElementAt(1).Command.Execute(null); 
            });
            return true;
        }
    }
}