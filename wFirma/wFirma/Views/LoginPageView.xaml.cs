﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Xml.Serialization;
using ModernHttpClient;
using Plugin.Settings;
using wFirma.Models;
using wFirma.ViewModels;

namespace wFirma.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPageView : ContentPage
	{
        
		public LoginPageView ()
		{
			InitializeComponent ();
		    
		}

	    protected override async void OnAppearing()
	    {
	        base.OnAppearing();
	        var user = CrossSettings.Current.GetValueOrDefault("username", "");
	        var password = CrossSettings.Current.GetValueOrDefault("password", "");
            if (CrossSettings.Current.GetValueOrDefault("remember", false).Equals(true))
	        {
	            UsernameEntry.Text = user;
	            PasswordEntry.Text = password;
	            RememberMeSwitch.IsToggled = true;
	            await Task.Delay(1000);
	            await Login(user, password);
	        }
        }

	    private async void LoginButton_OnClicked(object sender, EventArgs e)
	    {
	        try
	        {
	            if (!UsernameEntry.Text.Equals("") || UsernameEntry.Text != null)
	            {
	                if (!PasswordEntry.Text.Equals("") || PasswordEntry.Text != null)
	                {
	                    var username = UsernameEntry.Text;
	                    var password = PasswordEntry.Text;
	                    await Login(username, password);
	                }
	                else
	                {
	                    await DisplayAlert("Błąd", "Hasło nie może być puste!", "OK");
	                }
	            }
	            else
	            {
	                await DisplayAlert("Błąd", "Nazwa użytkownika nie może być pusta!", "OK");
	            }
	        }
	        catch (Exception ex)
	        {
	            await DisplayAlert("Błąd", "Proszę wprowadzić wszystkie dane!", "OK");
	        }
	        
	    }
	    private async Task Login(string username, string password)
	    {
	        var authData = string.Format("{0}:{1}", username, password);
	        var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));
	        HttpClient client = new HttpClient(new NativeMessageHandler());
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
	        try
	        {
                var response = await client.GetAsync("https://api2.wfirma.pl/users/get/178018?outputFormat=xml");
	            var generatedXml = await response.Content.ReadAsStringAsync();
                var objectResult = new UserItem();
	            if (generatedXml != null)
	            {
	                XmlSerializer serializer = new XmlSerializer(typeof(UserItem));
	                using (TextReader reader = new StringReader(generatedXml))
	                {
	                	objectResult = (UserItem)serializer.Deserialize(reader);
	                }
                }
	            App.userdata = objectResult.Users.User.Firstname + " " + objectResult.Users.User.Lastname;
                if (response.IsSuccessStatusCode)
	            {
	                if (RememberMeSwitch.IsToggled)
	                {
	                    CrossSettings.Current.AddOrUpdateValue("username", username);
	                    CrossSettings.Current.AddOrUpdateValue("password", password);
	                    CrossSettings.Current.AddOrUpdateValue("remember", true);
	                }
	                App.username = username;
	                App.password = password;
	                
	                await Navigation.PopModalAsync();
	                //LoginStatus.Text = "Zalogowany!";
	            }
            }
	        
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
	    }
	    protected override bool OnBackButtonPressed()
	    {
	        return true;
	    }
    }
}