﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using wFirma.Models;
using wFirma.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace wFirma.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OfferPDFView : ContentPage
    {
        public static string filename;
        public OfferPDFView(Invoice offer, OffersViewModel context)
        {
            InitializeComponent();
            context.PdfPreviewCommand.Execute(offer);
            pdfViewer.Uri = filename;
        }
    }
}