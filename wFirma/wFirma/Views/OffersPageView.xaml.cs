﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using wFirma.Models;
using wFirma.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace wFirma.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OffersPageView : ContentPage
    {
        private OffersViewModel ViewModel
        {
            get { return BindingContext as OffersViewModel; }
        }

        public OffersPageView()
        {
            InitializeComponent();
            BindingContext = new OffersViewModel()
            {
                Title = "Oferty",
                Icon = "offers.png",
                IsBusy = false,
                IsNotBusy = true
            };
            OffersListView.ItemTapped += (sender, args) =>
            {
                if (OffersListView.SelectedItem == null)
                    return;
                Invoice item = (Invoice)args.Item;
                ViewModel.SelectedOfferItem = item;
                UserDialogs.Instance.ActionSheet(new ActionSheetConfig().SetTitle("Wybierz jedną z opcji:")
                    .Add("Wyświetl ofertę", () => Navigation.PushAsync(new OfferView(item)))
                    .Add("Modyfikuj ofertę", () => Navigation.PushModalAsync(new NavigationPage(new OfferEditView(item, true, ViewModel))))
                    .Add("Podgląd PDF", () => Navigation.PushAsync(new OfferPDFView(item, ViewModel)))
                    .Add("Wystaw fakturę VAT", () => Navigation.PushModalAsync(new NavigationPage(new InvoiceEditView(item, false, new InvoicesViewModel()))))
                    .SetDestructive("Usuń ofertę", () => ViewModel.RemoveItemCommand.Execute(ViewModel.SelectedOfferItem.Id))
                    .SetCancel("Anuluj")
                    .SetUseBottomSheet(true));
                OffersListView.SelectedItem = null;
            };
            
            var newItem = new ToolbarItem
            {
                Text = "Nowa oferta",
                Command = new Command(() =>
                {
                    var item = new Invoice();
                    Navigation.PushModalAsync(new NavigationPage(new OfferEditView(item, false, ViewModel)));
                }),
                Icon = "addIcon2.png"
            };
            ToolbarItems.Add(newItem);
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (ViewModel == null || ViewModel.IsBusy || ViewModel.Offers.Count > 0)
                return;
            ViewModel.LoadItemsCommand.Execute(null);
        }
        protected override bool OnBackButtonPressed()
        {
            return true;
        }
    }
}