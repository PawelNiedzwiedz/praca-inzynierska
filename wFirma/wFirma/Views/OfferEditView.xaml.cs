﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Syncfusion.ListView.XForms;
using wFirma.CustomControls;
using wFirma.Models;
using wFirma.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace wFirma.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class OfferEditView : ContentPage
	{
	    private Image rightImage;
	    private Image leftImage;
	    private int itemIndex = -1;
	    private bool canGoBack = false;

        public OfferEditView(Invoice offer, bool editMode, OffersViewModel context)
        {
			InitializeComponent();
            if (offer.InvoiceContents == null)
            {
                offer.InvoiceContents = new Invoicecontents { Invoicecontent = new ObservableCollection<Invoicecontent>()};
                offer.Total = "0.00";
            }
            if (editMode)
            {
                foreach (var item in offer.InvoiceContents.Invoicecontent)
                {
                    item.VatCodeLabel = App.VatCodes.FirstOrDefault(x => x.Id.Equals(item.VatCode.Id)).Label;
                    item.ProductCount = (int)Convert.ToDouble(item.Count.Replace(".",","));
                }
            }
            var backupOffer = JsonConvert.DeserializeObject<Invoice>(JsonConvert.SerializeObject(offer));
            BindingContext = offer;

            var accept = new ToolbarItem
            {
                Text = "Zatwierdź",
                Command = new Command(async () =>
                {
                    if (offer.InvoiceContents.Invoicecontent.Count < 1 || offer.ContractorDetail == null)
                    {
                        await DisplayAlert("Błąd", "Proszę uzupełnić brakujące dane", "OK");
                        return;
                    }
                    if (editMode)
                    {
                        context?.EditItemCommand.Execute(offer.Id);
                    }
                    else
                    {
                        context?.AddItemCommand.Execute(offer);
                    }
                    await Navigation.PopModalAsync(true);
                })
            };
            var cancel = new ToolbarItem
            {
                Text = "Anuluj",
                Priority = 2,
                Command = new Command(async () =>
                {
                    var result = false;
                    if (editMode)
                    {
                        result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig()
                            .SetMessage("Czy chcesz przerwać edycję wybranego dokumentu?")
                            .SetTitle("Przerwij operację edycji")
                            .SetCancelText("Nie")
                            .SetOkText("Tak"));
                    }
                    else
                    {
                        result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig()
                            .SetMessage("Czy chcesz przerwać dodawanie nowego dokumentu?")
                            .SetTitle("Przerwij operację dodawania")
                            .SetCancelText("Nie")
                            .SetOkText("Tak"));
                    }
                    if (result)
                    {

                        if (context != null)
                        {
                            Invoice editedInvoice = context.Offers.Where(x => x.Id == backupOffer.Id).FirstOrDefault();
                            if (editedInvoice != null)
                            {
                                int indexOf = context.Offers.IndexOf(editedInvoice);
                                context.Offers.RemoveAt(indexOf);
                                context.Offers.Insert(indexOf, backupOffer);
                                canGoBack = true;
                            }
                        }
                        await Navigation.PopModalAsync(true);
                    }
                }),
            };
            ToolbarItems.Add(accept);
            ToolbarItems.Add(cancel);
        }

	    private async void AddContractorButton_OnClicked(object sender, EventArgs e)
	    {
	        var contractorsPageView = new ContractorsPageView(true);
            contractorsPageView.Disappearing += ContractorsPageView_Disappearing;
            await Navigation.PushAsync(contractorsPageView);
	    }

        private void ContractorsPageView_Disappearing(object sender, EventArgs e)
        {
            ContractorsPageView page = sender as ContractorsPageView;
            if (page?.SelectedContractor != null)
            {
                ((Invoice)this.BindingContext).ContractorDetail = (ContractorDetail)page.SelectedContractor;
            }
        }

	    private async void AddProduct_OnClicked(object sender, EventArgs e)
	    {
	        var productsPage = new ProductsPageView(true);
            productsPage.Disappearing += ProductsPage_Disappearing;
	        await Navigation.PushAsync(productsPage);
	    }

        private void ProductsPage_Disappearing(object sender, EventArgs e)
        {
            ProductsPageView page = sender as ProductsPageView;
            if (page != null)
            {
                ((Invoice)this.BindingContext).InvoiceContents.Invoicecontent.Add((Invoicecontent)page.SelectedProduct);
                ((Invoice)this.BindingContext).Total = Total();
            }
        }
	    private void rightImage_BindingContextChanged(object sender, EventArgs e)
	    {
	        if (rightImage == null)
	        {
	            rightImage = sender as Image;
	            (rightImage.Parent as View).GestureRecognizers.Add(new TapGestureRecognizer()
	            {
	                Command = new Command (() =>
	                {
	                    ((Invoice)this.BindingContext).InvoiceContents.Invoicecontent.RemoveAt(itemIndex);
	                    ((Invoice) this.BindingContext).Total = Total();
                        ProductListView.ResetSwipe();
                    })
                });
            }
	    }

	    private void ProductListView_OnSwipeEnded(object sender, SwipeEndedEventArgs e)
	    {
	        itemIndex = e.ItemIndex;
	    }

	    private void leftImage_BindingContextChanged(object sender, EventArgs e)
	    {
	        if (leftImage == null)
	        {
	            leftImage = sender as Image;
	            (leftImage.Parent as View).GestureRecognizers.Add(new TapGestureRecognizer()
	            {
	                Command = new Command(() =>
	                {
	                    var error = false;
	                    try
	                    {
	                        EditProduct();
                            ProductListView.ResetSwipe();
	                    }
	                    catch (Exception ex)
	                    {
	                        error = true;
	                    }
	                    if (error)
	                    {
	                        var page = new ContentPage();
	                        var result = page.DisplayAlert("Błąd", "Wystąpił błąd podczas modyfikacji pozycji na dokumencie.", "OK");
                        }
	                })
	            });
	        }
	    }
	    protected override bool OnBackButtonPressed()
	    {
	        if (canGoBack)
	        {
	            return base.OnBackButtonPressed();
	        }
	        Device.BeginInvokeOnMainThread(() =>
	        {
	            ToolbarItems.ElementAt(1).Command.Execute(null);
	        });
	        return true;
	    }

	    public static Task<string> InputBox(INavigation navigation)
	    {
	        // wait in this proc, until user did his input 
	        var tcs = new TaskCompletionSource<string>();

	        var lblMessage = new Label { Text = "Wprowadź ilość:" };
	        var txtInput = new Entry { Text = "", Keyboard = Keyboard.Numeric};

	        var btnOk = new Button
	        {
	            Text = "Zatwierdź",
	            WidthRequest = 100,
	            BackgroundColor = Color.FromRgb(0.8, 0.8, 0.8),
	        };
	        btnOk.Clicked += async (s, e) =>
	        {
	            // close page
	            var result = txtInput.Text;
	            result += ".0";
	            await navigation.PopModalAsync();
	            // pass result
	            tcs.SetResult(result);
	        };

	        var btnCancel = new Button
	        {
	            Text = "Anuluj",
	            WidthRequest = 100,
	            BackgroundColor = Color.FromRgb(0.8, 0.8, 0.8)
	        };
	        btnCancel.Clicked += async (s, e) =>
	        {
	            // close page
	            await navigation.PopModalAsync();
	            // pass empty result
	            tcs.SetResult(null);
	        };

	        var slButtons = new StackLayout
	        {
	            Orientation = StackOrientation.Horizontal,
	            Children = { btnOk, btnCancel },
	        };

	        var layout = new StackLayout
	        {
	            Padding = new Thickness(0, 40, 0, 0),
	            VerticalOptions = LayoutOptions.CenterAndExpand,
	            HorizontalOptions = LayoutOptions.CenterAndExpand,
	            Orientation = StackOrientation.Vertical,
	            Children = {lblMessage, txtInput, slButtons },
	        };

	        // create and show page
	        var page = new ContentPage();
	        page.Content = layout;
	        navigation.PushModalAsync(page);
	        // open keyboard
	        txtInput.Focus();

	        // code is waiting her, until result is passed with tcs.SetResult() in btn-Clicked
	        // then proc returns the result
	        return tcs.Task;
	    }

	    public string Total()
	    {
	        double totalCash = 0.0;
	        foreach (var item in ((Invoice)this.BindingContext).InvoiceContents.Invoicecontent)
	        {
	            var itemBrutto = Convert.ToDouble(item.Brutto.Replace(".",","));
	            totalCash += itemBrutto;
	        }
	        return totalCash.ToString("N").Replace(",",".");
	    }

	    public async void EditProduct()
	    {
	        try
	        {
	            var result = await InputBox(this.Navigation);
	            if (result == null) return;
	            result = result.Substring(0, result.IndexOf(".", StringComparison.Ordinal));
	            var count = Convert.ToInt32(result);
	            ((Invoice) this.BindingContext).InvoiceContents.Invoicecontent.ElementAt(itemIndex).Count =
	                result;
	            ((Invoice) this.BindingContext).InvoiceContents.Invoicecontent.ElementAt(itemIndex)
	                .ProductCount = count;
	            var productNetto = ((Invoice) this.BindingContext).InvoiceContents.Invoicecontent
	                .ElementAt(itemIndex).Price.Replace(".", ",");
	            var netto = Convert.ToDouble(productNetto);
	            var vatLabel = ((Invoice) this.BindingContext).InvoiceContents.Invoicecontent
	                .ElementAt(itemIndex).VatCodeLabel;
	            var vat = Convert.ToDouble(
	                vatLabel.Substring(0, vatLabel.IndexOf("%", StringComparison.Ordinal)));
	            vat = vat / 100;
	            var brutto = (netto * vat) + netto;
	            var totalBrutto = brutto * count;
	            ((Invoice) this.BindingContext).InvoiceContents.Invoicecontent.ElementAt(itemIndex).Brutto =
	                totalBrutto.ToString("N").Replace(",", ".");
	            ((Invoice) this.BindingContext).Total = Total();

	        }
	        catch (Exception ex)
	        {
	            Debug.WriteLine(ex.Message);
	        }
	        
        }
	}
}

