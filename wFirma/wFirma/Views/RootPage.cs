﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Settings;
using wFirma.Models;
using wFirma.ViewModels;
using Xamarin.Forms;

namespace wFirma.Views
{
    public class RootPage : MasterDetailPage
    {
        private Dictionary<MainMenuType, NavigationPage> MainMenuPages { get; set; }

        public RootPage()
        {
            MainMenuPages = new Dictionary<MainMenuType, NavigationPage>();
            Master = new MainMenuPageView(this);
            
            BindingContext = new MenuViewModel
            {
                Title = "Menu",
                Icon = ""
            };
            ShowLoginPage(false);
            NavigateAsync(MainMenuType.HomePage);
            InvalidateMeasure();
            
        }

        public async void ShowLoginPage(bool logout)
        {
            if (logout)
            {
                CrossSettings.Current.AddOrUpdateValue("remember", false);
            }
            var page = new LoginPageView();
            await Navigation.PushModalAsync(page);
            page.Disappearing += Page_Disappearing;
        }

        private void Page_Disappearing(object sender, EventArgs e)
        {
            var viewmodel = new HomePageViewModel();
            viewmodel.GetVatCodesCommand.Execute(null);
        }

        public async Task NavigateAsync(MainMenuType id)
        {
            Page newPage;
            if (!MainMenuPages.ContainsKey(id))
            {
                switch (id)
                {
                    case MainMenuType.HomePage:
                        MainMenuPages.Add(id, new NavigationPage(new HomePageView()));
                        break;
                    case MainMenuType.Contractors:
                        MainMenuPages.Add(id, new NavigationPage(new ContractorsPageView(false)));
                        break;
                    case MainMenuType.Products:
                        MainMenuPages.Add(id, new NavigationPage(new ProductsPageView(false)));
                        break;
                    case MainMenuType.Invoices:
                        MainMenuPages.Add(id, new NavigationPage(new InvoicesPageView()));
                        break;
                    case MainMenuType.Offers:
                        MainMenuPages.Add(id, new NavigationPage(new OffersPageView()));
                        break;

                }
            }
            newPage = MainMenuPages[id];
            if (newPage == null)
            {
                return;
            }
            Detail = newPage;
            IsPresented = false;
        }
    }
}
